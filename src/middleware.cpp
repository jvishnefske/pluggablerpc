/**
 * @file    Connection
 * @author  John Vishnefske
 * @date    11/19/21
 * @brief   middleware
 *
 */
#include "middleware.h"

#include <simdjson.h>

#include <array>
#include <boost/asio.hpp>
#include <boost/asio/co_spawn.hpp>
#include <boost/asio/awaitable.hpp>
#include <boost/beast.hpp>
#include <boost/beast/_experimental/test/stream.hpp>
#include <boost/beast/_experimental/test/tcp.hpp>
#include <boost/beast/core.hpp>
//#include <boost/beast/ssl.hpp>
#include <boost/beast/websocket/stream_fwd.hpp>
#include <functional>
#include <memory>
#include <nlohmann/json.hpp>
#include <set>
#include <string>
#include <type_traits>
#include <unordered_map>

#include "queue.h"
using string_view = boost::string_view;

/**
 *  quic raii warpper to c api pointer
 */
#if ENABLE_MSQUIC
namespace middleware {
class MsQuicRegistration {
   private:
    QUIC_REGISTRATION_CONFIG m_config = {
        .AppName = "middleware",
        .ExecutionProfile = QUIC_EXECUTION_PROFILE_LOW_LATENCY};

   public:
    const QUIC_API_TABLE *api;
    QUIC_HANDLE *m_registration;
    QUIC_HANDLE *m_listener = nullptr;

    MsQuicRegistration();

    //~MsQuicRegistration();

    /*
     *
     */
    QUIC_HANDLE **GetListenerHandle();
};

}  // namespace middleware
/*----------------------------------------------------------------------------*/
middleware::MsQuicRegistration::MsQuicRegistration() {
    const QUIC_API_TABLE *localApi;
    auto status = MsQuicOpen(&localApi);
    api = localApi;
    if (QUIC_FAILED(status)) {
        std::cout << "MsQuicOpen failed: " << status << std::endl;
        std::runtime_error("MsQuicOpen failed");
    }

    // outputs m_registration, so we can only have one.
    status = api->RegistrationOpen(&m_config, &m_registration);
    if (QUIC_FAILED(status)) {
        throw std::runtime_error("MsQuicOpenRegistration failed ");
    }
}
middleware::MsQuicRegistration::~MsQuicRegistration() {
    if (m_listener) {
        api->ListenerClose(m_listener);
    }
    api->RegistrationClose(m_registration);
    MsQuicClose(api);
}
QUIC_HANDLE **middleware::MsQuicRegistration::GetListenerHandle() {
    if (m_listener == nullptr) {
        auto status = api->ListenerOpen(m_registration, ServerListenerCallback,
                                        nullptr, &m_listener);
        if (QUIC_FAILED(status)) {
            std::cout << "ListenerOpen failed: " << status << std::endl;
            std::runtime_error("ListenerOpen failed");
        }
    }
    return &m_listener;
}
/**
 *         const QUIC_BUFFER m_buffer = {
                .Length = static_cast<uint32_t>(m_array.size()),
                .Buffer = m_array.data()
        };
 * @param port
 * @param url
 */
middleware::MsQuicServer::MsQuicServer(int port, std::string url) {
    // todo set configuration.
    QUIC_ADDR Address = {0};
    // Address.Ipv4.sin_port = htons(port);
    Address.Ipv4.sin_family = AF_INET;
    //            Address.Ipv4.sin_addr.s_addr =
    //                    inet_pton(AF_INET, url.c_str(), &Address.Ipv4.sin_addr
    //                    == 1 ? QUIC_ADDRESS_FAMILY_INET4 :
    //                    QUIC_ADDRESS_FAMILY_INET6);
    // Address.Ipv4.sin_addr.s_addr = htonl(INADDR_ANY);
    // auto status = m_quic->api->ListenerOpen(m_quic->m_registration,
    // ServerListenerCallback, nullptr,  m_quic->GetListenerHandle());
    if (!m_quic) {
        throw std::runtime_error(
            "MsQuicOpenRegistration failed: null pointer detected in server");
    }

    //    if (QUIC_FAILED(status)) {
    //        std::cout << "ListenerOpen failed: " << status << std::endl;
    //        std::runtime_error("ListenerOpen failed");
    //    }
    //            status = m_quic->api->ListenerBind(m_listener, &Address);
}
#endif
// the non quic version of WebsocketServer/WebSocketClient

namespace middleware {
namespace net = boost::asio;
namespace beast = boost::beast;
using WStream = boost::beast::websocket::stream<boost::beast::tcp_stream>;
using TestStream = boost::beast::test::stream;
using StreamVariant = variant<
    WStream
    /*, beast::websocket::stream<beast::ssl_stream<beast::tcp_stream>>*/>;
using Stream = WStream;

template <typename Token>
auto async_read(Stream &stream, Token const &token) {
    std::visit(
        [&token, &stream](auto const &s) { s.async_read(stream, token); },
        stream);
    //    if constexpr(std::holds_alternative<WStream>(stream)){
    //        std::get<WStream>(stream).async_read_some()
    //    }
}
using AsyncWritable = std::function<void()>;
template <class MessageParser>
class BasicMessageRouter {
   public:
    template <class InputStream>
    void do_read(InputStream &stream) {
        stream.async_read();
    }

    // we need a way to convert this stream into a variant stream so that a
    // functiona can write to muttiple output streams in a container.

   private:
    template <class InputStream>
    void on_read(boost::beast::error_code /*ec*/, size_t /*byte_count*/,
                 InputStream & /*stream*/) {}
    std::unordered_map<std::string, std::set<WStream>> m_subscribers_by_topic;
    std::unordered_map<std::string, std::pair<WStream, std::string>>
        m_announcements_by_topic;
};
using MessageRouter = BasicMessageRouter<StreamJsonReaderImpl>;
using Error = int;  // boost::beast::error_code;
using BufferType = std::string_view;

/**
 * interface bewteen sequence of bytes, and sequence of message classes.
 */
template <class... PublishableTypes>
struct MessageAdapter {
    using AnnounceListner = std::function<void(AnnounceMessage)>;
    using SubscribeListner = std::function<void(AnnounceListner)>;
    using PublishListner =
        std::function<void(PublishMessage<PublishableTypes...>)>;

    //    AsyncWriter async_write;
    //    AsyncReader async_read;
    //    AnnounceListner announceListner;
    //    PublishListner publishListner;
    //    SubscriberListner subscriberListner;
};
#if 0
class GenericConnection {
    using AsyncWriter =
        std::function<void(BufferType, std::function<void(Error)>)>;
    using AsyncReader =
        std::function<void(std::function<void(Error, BufferType)>)>;
    template <class MessageAdapter>
    GenericConnection(MessageAdapter adapt) {

    }
};
#endif
/**
 * iterate_many currently operates on a padded_string, or  pointer, and length.
 * when passed a padded_string_view this
 * @param p
 * @return
 */
simdjson::ondemand::document_stream iterate_many(
    simdjson::ondemand::parser &p,
    boost::beast::flat_static_buffer_base &buffer) {
    buffer.prepare(simdjson::SIMDJSON_PADDING);
    return p.iterate_many(static_cast<char *>(buffer.data().data()),
                          buffer.data().size(),
                          simdjson::ondemand::DEFAULT_BATCH_SIZE);
}
simdjson::padded_string_view to_sv(boost::beast::flat_static_buffer_base &buf) {
    net::const_buffer cbuf = buf.data();
    // prepare_impl throws std::length_error if the buffer is not large enough.
    auto write_buffer = buf.prepare(simdjson::SIMDJSON_PADDING * 2);
    simdjson::padded_string_view padded{static_cast<const char *>(cbuf.data()),
                                        cbuf.size(),
                                        cbuf.size() + write_buffer.size()};
    return padded;
}

}  // namespace middleware

// to be refactored.
namespace {
using BinaryData = std::vector<char>;

template <class StreamFactory>
class Publisher;

class QuicStream;

class QuicDatagram;
// need to consider an interface which implements generic transport.
class WebsocketClient;
class WebsocketServer;

using PtrQuicDatagram = std::shared_ptr<QuicDatagram>;
using PtrQuicStream = std::shared_ptr<QuicStream>;

// can be shared by two classes one of which sends, and one of which pushes
// responses. This is relevant for both server, and client libraries, and also
// Server
class SingleStream {
   public:
    std::function<void(BinaryData)> ReceiveCallback;
    std::function<void(BinaryData)> SendCallback;
};

enum Qos {
    qLowPriority  // low priority stream
};
enum Reliability {
    qReliable,  // use streams
                // qBestEffort;   // use datagram
};
enum Durability {
    // qTransientLocal; // keeps track of last sample for late subscribers
    qVolatile  // no persistance of samples.
};

class JsonHandler {
   private:
    // map topics to callbacks
    std::map<std::string, std::function<void(json_node)>> subscribers;

   public:
    void operator()(BinaryData data);

    void addSubscriber(std::string topic,
                       std::function<void(json_node)> callback) {
        subscribers[topic] = callback;
    }

    void removeSubscriber(std::string topic) { subscribers.erase(topic); }
};

template <class Handler, class ServerImpl>
class ServerInterface {
   private:
    std::shared_ptr<ServerImpl> m_server;
    std::function<void(BinaryData)> m_handlerCallback;

   public:
    ServerInterface() : m_server() {}
    //        void bind(int port,std::string url);
    class dummy {};
    // SingleStream
    void setConnectionHandler(std::function<void(BinaryData)> handler) {
        m_handlerCallback = handler;
    }
};

class MsQuicServer {
   private:
    std::shared_ptr<MsQuicRegistration> m_quic;
    std::function<void(std::vector<char>)> m_connectionHandler;
    std::array<uint8_t, 1024> m_array;

   public:
    MsQuicServer(int port = 8000, std::string url = "0.0.0.0");
    void setConnectionHandler(
        const decltype(SingleStream::ReceiveCallback) connectionHandler) {
        m_connectionHandler = connectionHandler;
    }
};
using MsQuicError = std::runtime_error;

// using QuicServer = QuicServerImpl<JsonHandler>;

class MsQuicClient {
   private:
    std::shared_ptr<MsQuicRegistration> m_quic;

   public:
    MsQuicClient(int port = 8000, std::string url = "localhost") {
        std::cout << "MsQuicClient" << url << ":" << port << std::endl;
    }
};
template <class data_type>
class Publisher {
   private:
    using MyServerImpl = WebSocket;
    ServerInterface<data_type, MyServerImpl> m_server;

   public:
    Publisher(int port = 9000) : m_server(port) {}
    void publish(data_type data) { m_server.setConnectionHandler(data); }
};
#if 0
void JsonHandler::operator()(BinaryData data) {
    // parse binary data into json
    nlohmann::json j = nlohmann::json::parse(data.data());
    std::cout << "JsonHandler: " << data.data() << std::endl;
}
#endif
template <class T>
void LOG(const T s) {
    std::cout << s;
}
/*template<class... T>
void LOG(const T... i){
    {LOG(i);...};
}*/
}  // namespace

namespace middleware {

/**
 * there is at least road block with implementing the read_some as a function,
 * so we encapsulate the state here.
 */
class StreamJsonReaderImpl {
   private:
    enum : size_t { bufferSize = 8192, default_read_length = 4096 };
    boost::beast::flat_static_buffer<bufferSize + simdjson::SIMDJSON_PADDING>
        m_read_buffer{};
    simdjson::ondemand::parser m_parser;
    simdjson::ondemand::document_stream m_doc_stream;
    simdjson::ondemand::document_stream::iterator m_doc_iterator;
    int m_bytesReadyToConsume = 0;
    // set this true if there is an item in the buffer which needs to be ereased
    // before we return a new document.
    void consume();
    size_t size_to_try() {
        auto free_space = m_read_buffer.max_size() - m_read_buffer.size() -
                          simdjson::SIMDJSON_PADDING;
        return std::min(static_cast<decltype(free_space)>(default_read_length),
                        free_space);
    }
    void do_read() {}

   public:
    explicit StreamJsonReaderImpl()
        : m_read_buffer{},
          m_parser{},
          m_doc_stream{m_parser.iterate_many(middleware::to_sv(m_read_buffer))},
          m_doc_iterator{m_doc_stream.begin()} {}

    /**
     * True if there is a document internally available
     * without additional read from stream.
     * @return
     */
    bool isDocInBuffer() { return m_doc_iterator != m_doc_stream.end(); }
    template <class next_layer>
    void async_write(next_layer &s,
                     middleware::PartiallyParsedMessage message) {
        return s.write(net::buffer(message.messageView));
    }
    /*
     * returns one document, or an error.
     * read from stream until a json meessage is received.
     * there may be bytes after the json message. which are left in the buffer.
     * The dynamic buffer should be preserved between calls to this function.
     */
    template <class next_layer>
    void read_some(next_layer &s, simdjson::ondemand::document_reference doc) {
        std::string_view unused;
        read_some(std::ref(s), doc, unused);
    }
    template <class next_layer>
    void read_some(next_layer &s, simdjson::ondemand::document_reference doc,
                   std::string_view &rawMessage) {
        const auto max_read_attempts = 3;
        int read_attempts = 0;
        while (!(m_doc_iterator != m_doc_stream.end())) {
            // we are at the end of the document stream
            // 1 try to consume
            m_read_buffer.consume(m_bytesReadyToConsume);
            m_bytesReadyToConsume = 0;
            // todo add error checking
            int read_length = s.read_some(m_read_buffer.prepare(size_to_try()));
            m_read_buffer.commit(read_length);
            ++read_attempts;
            if (read_attempts > max_read_attempts)
                LOG("read seems to be going nowhere.");
            m_doc_stream =
                m_parser.iterate_many(middleware::to_sv(m_read_buffer));
            m_doc_iterator = m_doc_stream.begin();
        }
        // should be ready to return now
        doc = *m_doc_iterator;
        // The name is a bit confusing, but this is the number of bytes
        // to consume after parsing this document is complete.
        rawMessage = m_doc_iterator.source();
        m_bytesReadyToConsume = m_doc_iterator.source().size();
        ++m_doc_iterator;
    }
};
// class to hold both server and client streams for processing my a pubsub
// message router.
#define USE_ASIO_FAUXROUTINE 1
#ifdef USE_ASIO_FAUXROUTINE
class MessageStream /*: public std::enable_shared_from_this<MessageStream>*/ {
   public:
    // beast::websocket::stream<beast::tcp_stream>
    explicit MessageStream(beast::websocket::stream<beast::tcp_stream> &&stream)
        : m_stream(std::move(stream)),
          //              std::in_place_type<beast::websocket::stream<beast::tcp_stream>>,
          //              std::move(stream)),
          m_parser{},
          m_docStream{m_parser.iterate_many(middleware::to_sv(m_readBuffer))},
          m_docIterator{m_docStream.begin()} {}
    //    MessageStream(net::io_context &context)
    //        : m_stream(std::in_place_type<beast::test::stream>, context),
    //          m_parser{},
    //          m_docStream{m_parser.iterate_many(middleware::to_sv(m_readBuffer))},
    //          m_docIterator{m_docStream.begin()} {}

    template <typename CompletionToken>
    auto async_read_message(CompletionToken &&token) {
        m_coRead = net::coroutine();
        if (m_stream.index() == 0) {
            return net::async_compose<CompletionToken,
                                      void(beast::error_code,
                                           PartiallyParsedMessage)>(
                [&](auto &completion) {
                    return co_read(completion, beast::error_code{}, 0);
                },
                token, std::get<0>(m_stream));
        } else {
            throw std::runtime_error(
                "variant_alternative reading not yet support in "
                "co_read");
        }
    }

    template <typename CompletionToken>
    auto async_write_some(const std::string output_message,
                          CompletionToken &&token) {
        (void) output_message;
        (void) token;
#if 0
        return std::visit(
            [&token, output_message, self=shared_from_this()](auto socket) {
                if constexpr (std::is_same_v<decltype(socket),
                                             middleware::WStream>) {
                    return net::async_write(
                        socket, net::buffer(output_message),
                        std::forward<CompletionToken>(token));
                } else if constexpr (std::is_same_v<
                                         decltype(socket),
                                         std::variant_alternative<
                                             1, decltype(self->m_stream)>>) {
                    return net::async_write(
                        socket, net::buffer(output_message),
                        std::forward<CompletionToken>(token));
                }else{
                    throw std::runtime_error("variant not exhaustive in middleware async_write_some.");
                }
            },
            m_stream);
#endif
    }

    //    void append(std::string message) {
    //        std::get<beast::test::stream>(m_stream).append(message);
    //    }
    template <class Self>
    void co_read(Self &completion, beast::error_code error,
                 size_t read_length) {
        // reenterable function to compose async read into
        if (m_readyToIncrement) {
            ++m_docIterator;
            m_readyToIncrement = false;
        }
#if 0
        auto read_visitor = [self = shared_from_this(),
                             &completion](auto stream) {
            /*            if constexpr (std::is_same_v<
                                          decltype(stream),
                                          beast::websocket::stream<beast::tcp_stream>>)
               {*/
            return stream.async_read_some(
                self->m_readBuffer.prepare(self->size_to_try()),
                std::bind(&MessageStream::co_read, self, completion,
                          std::placeholders::_1, std::placeholders::_2));
            //            }
        };
        auto reader = [self = shared_from_this(), read_visitor]() {
            return std::visit(read_visitor, self->m_stream);
        };
#endif
        BOOST_ASIO_CORO_REENTER(m_coRead) {
            while (!(m_docIterator != m_docStream.end())) {
                m_readBuffer.consume(m_bytesReadyToConsume);
                m_bytesReadyToConsume = 0;
                if (m_stream.index() == 0) {
                    BOOST_ASIO_CORO_YIELD std::get<0>(m_stream).async_read_some(
                        m_readBuffer.prepare(size_to_try()),
                        [&](beast::error_code ec, size_t read_length) {
                            return co_read(completion, ec, read_length);
                        });
                } else {
                    throw std::runtime_error(
                        "variant_alternative reading not yet support in "
                        "co_read");
                }
                if (error) {
                    // todo: handle error without exception.
                    throw std::runtime_error("Error reading message stream");
                }
                m_readBuffer.consume(read_length);
                m_docStream =
                    m_parser.iterate_many(middleware::to_sv(m_readBuffer));
                m_docIterator = m_docStream.begin();
            }
            m_bytesReadyToConsume = m_docIterator.source().size();
            m_readyToIncrement = true;  // mark iterator as ready to consume.

            // todo populate message here.
            completion.complete(beast::error_code{}, PartiallyParsedMessage{});
        }
    }

   private:
    template<class Stream>
    auto read_visitor(Stream stream) {
        return stream.async_read_some(m_readBuffer.prepare(size_to_try()));
    }
    size_t size_to_try() {
        size_t free_space = m_readBuffer.max_size() - m_readBuffer.size() -
                            simdjson::SIMDJSON_PADDING;
        return std::min<size_t>(default_read_length, free_space);
    }
    StreamVariant m_stream;
    enum : size_t { bufferSize = 8192, default_read_length = 4096 };
    beast::flat_static_buffer<bufferSize> m_readBuffer;
    net::coroutine m_coRead;
    simdjson::ondemand::parser m_parser;
    simdjson::ondemand::document_stream m_docStream;
    simdjson::ondemand::document_stream::iterator m_docIterator;
    std::size_t m_bytesReadyToConsume = 0;
    bool m_readyToIncrement = false;
};
#else
class MessageStream {
   public:
    MessageStream(beast::websocket::stream<beast::tcp_stream> &&stream)
        : m_stream{std::move(stream)} {}
    net::awaitable<PartiallyParsedMessage> co_read_message() {
        // do one read, and return a message. This does not yet support
        // async_read_some buffers not on json boundries.
        std::size_t n = co_await m_stream.async_read_some(
            m_readBuffer.prepare(size_to_try()), net::use_awaitable);
        m_readBuffer.commit(n);
        m_docStream = m_parser.iterate_many(middleware::to_sv(m_readBuffer));
        m_docIterator = m_docStream.begin();
        co_return parseIterator();
    }

   private:
    size_t size_to_try() {
        size_t free_space = m_readBuffer.max_size() - m_readBuffer.size() -
                            simdjson::SIMDJSON_PADDING;
        return std::min<size_t>(default_read_length, free_space);
    }
    middleware::PartiallyParsedMessage parseIterator() {
        PartiallyParsedMessage result;
        result.messageView = m_docIterator.source();
        simdjson::ondemand::document_reference doc = *m_docIterator;
        std::string_view op;
        auto error = doc.find_field("op").get(op);
        if (error != simdjson::error_code::SUCCESS) {
            throw std::runtime_error("Error parsing message");
        }
        if (op == "announce") {
            AnnounceMessage a;
            doc.find_field("topic").get(a.topic);
            doc.find_field("type").get(a.type);
            result.messageFields = a;
        } else if (op == "publish") {
            RawPublishMessage p;
            doc.find_field("topic").get(p.topic);
            result.messageFields = p;
        } else if (op == "subscribe") {
            SubscribeMessage s;
            doc.find_field("topic").get(s.topic);
            doc.find_field("type").get(s.type);
            result.messageFields = s;
        } else {
            throw std::runtime_error("invalid message operation");
        }
        return result;
    }
    enum : size_t { bufferSize = 8192, default_read_length = 4096 };
    beast::websocket::stream<beast::tcp_stream> m_stream;
    beast::flat_static_buffer<bufferSize> m_readBuffer;
    simdjson::ondemand::parser m_parser;
    simdjson::ondemand::document_stream m_docStream;
    simdjson::ondemand::document_stream::iterator m_docIterator;
};
#endif
}  // namespace middleware
