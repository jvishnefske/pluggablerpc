//
// Created by John Vishnefske on 11/22/21.
//

#ifndef MIDDLEWARE_QUEUE_H
#define MIDDLEWARE_QUEUE_H
#include <condition_variable>
#include <future>
#include <iostream>
#include <mutex>
#include <queue>
#include <vector>

class Semaphore {
   public:
    void wait() {
        std::unique_lock<std::mutex> lock(m_mutex);
        while (m_count == 0) {
            m_condition.wait(lock);
        }
        m_count--;
    }
    void signal() {
        std::unique_lock<std::mutex> lock(m_mutex);
        m_count++;
        m_condition.notify_one();
    }

   private:
    std::mutex m_mutex;
    std::condition_variable m_condition;
    int m_count;
};
template <class T>
class BlockingQueue {
   public:
    T dequeue() {
        std::unique_lock<std::mutex> lock(m_mutex);
        while (m_queue.empty()) {
            m_condition.wait(lock);
        }
        T value = m_queue.front();
        m_queue.pop();
        return value;
    }
    void enqueue(T value) {
        std::unique_lock<std::mutex> lock(m_mutex);
        m_queue.push(value);
        m_condition.notify_one();
    }

   private:
    std::mutex m_mutex;
    std::condition_variable m_condition;
    std::queue<T> m_queue;
};

/**
 * measure context time. note that this should be updated for rule of five.
 */
using Clock = std::chrono::system_clock;
class ContextTimer {
   public:
    ContextTimer() { tic(); }
    ~ContextTimer() { toc(); }
    void tic() { m_startTime = Clock::now(); }
    void toc() {
        auto endTime = Clock::now();
        std::cout << "elapsed: "
                  << std::chrono::duration_cast<std::chrono::milliseconds>(
                         endTime - m_startTime)
                         .count()
                  << "ms" << std::endl;
    }

   private:
    std::chrono::time_point<Clock> m_startTime;

   public:
    ContextTimer(const ContextTimer &) {
        std::cout << "copy constructor" << std::endl;
    }
    //    ContextTimer& operator=(const ContextTimer& t){
    //        std::cout << "copy assignment" << std::endl;
    //
    //    }
    // move constructor
   public:
    ContextTimer(ContextTimer &&) {
        std::cout << "move constructor" << std::endl;
    }
    //    ContextTimer& operator=(ContextTimer&&){
    //        std::cout << "move assignment" << std::endl;
    //    }
};

// raii wrapper for a set of std::function observers
template <class T>
class ObserverSet {
   private:
    std::vector<std::function<void(T)>> m_observers;
    std::vector<std::future<T>> m_futures;

   public:
    template <class F>
    void then(std::function<F(T)> f) {
        m_observers.push_back(
            [&f](T &&t) { return std::move(f(std::move(t))); });
    }

    void add(std::function<void(T)> f) { m_observers.push_back(f); }

    void notify(T value) {
        for (auto &f : m_observers) {
            m_futures.push_back(std::async(std::launch::async, f, value));
        }
    }

    void operator()(T value) {
        std::vector<std::future<T>> futures;
        for (auto &observer : m_observers) {
            futures.push_back(std::async(std::launch::async, observer, value));
        }
    }
};

#endif  // include guard