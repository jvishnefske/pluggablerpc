/**
 * @file   middleware.h
 * @author  John Vishnefske <jvishnefske@acm.org>
 * @date    11/19/21
 * @brief   Provides a simple interface to the http libraries.
 * Allows for pubsub, command result, and request/response.
 * future consideration include unreliable connections for low latency realtime
 * requirements. This allows exploring the performance of RFC9000 QUIC in
 * various scenarios.
 */

// todo make a client, and server connection ☹.
#ifndef MIDDLEWARE_MIDDLEWARE_H
#define MIDDLEWARE_MIDDLEWARE_H
#include <iosfwd>
#include <string>
#include <variant>
#include <functional>
#include <memory>
// needed to support std c++14 and earlier
#include <boost/utility/string_view.hpp>
#include <boost/variant2/variant.hpp>
class json_node;
class MsQuicRegistration;
class WebSocket;

namespace middleware {
class StreamJsonReaderImpl;

    using namespace boost::variant2;
    using namespace boost;
// see:
// https://github.com/RobotWebTools/rosbridge_suite/blob/groovy-devel/ROSBRIDGE_PROTOCOL.md
// other message opperations include unsubscribe, call_service,
// service_response, auth status, unadvertize, set_level
using String = string_view;
struct SubscribeMessage {
    String type;
    String topic;
    static std::string op() { return "subscribe"; };
};
struct AnnounceMessage {
    String type;
    String topic;
    static std::string op() { return "announce"; };
};
struct RawPublishMessage {
//  String msg;
  String topic;
  static String op() { return "publish";}
};
template <class... Types>
struct PublishMessage {
    const variant<Types...> msg;
    const String topic;
    static std::string op() { return "publish"; };
};
// string view points to the read buffer, and is invalidated when a new read is performed on the relevant stream.
// this may help performance in the case where all messages are written before a new read is performed.
struct PartiallyParsedMessage{
    string_view messageView; //
    variant<monostate, SubscribeMessage, AnnounceMessage, RawPublishMessage> messageFields;
};
enum class StreamType{kTestStream, kTcpStream};
}  // namespace middleware
#endif  // MIDDLEWARE_MIDDLEWARE_H
