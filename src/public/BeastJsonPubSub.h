//
// Created by John Vishnefske on 6/3/2022.
//

#ifndef MIDDLEWARE_BEASTJSONPUBSUB_H
#define MIDDLEWARE_BEASTJSONPUBSUB_H

#include <nlohmann/json.hpp>
#include <boost/beast/websocket/stream_fwd.hpp>
#include <boost/asio/ts/netfwd.hpp>
#include <iostream>
// the plan is to initially create an end to end solution in order to consider
// the feasablilty of making pluggable transport, and serialization layers.

//namespace asio = boost::asio;
//using json = nlohmann::json;
//struct ObjectToObserve {
//    int data;
//    double float_data;
//};
//
//// implicit json conversion
//inline void to_json(json &j, const ObjectToObserve &obj)
//{
//    j = json{{"data",       obj.data},
//             {"float_data", obj.float_data}};
//}
//
//void from_json(const json &j, ObjectToObserve &obj)
//{
//    j.at("data").get_to(obj.data);
//    j.at("float_data").get_to(obj.float_data);
//}

#if 0

template<class T>
class Topic {
public:
    Topic(const std::string &name, const std::string &type)
            : name(name), type(type), description(description)
    {
    }

    void incomingMessage(const std::string &str)
    {
        nlohmann::json j = nlohmann::json::parse(str);
        const auto obj = j.get<T>();
        std::cout << "incomingMessage: " << obj.data << std::endl;
        callback(obj);
    }

    void publish(const T &obj)
    {
        nlohmann::json j = obj;
        std::cout << "publish: " << j.dump() << std::endl;
        transmitMessageCallback(j.dump());
    }

private:
    std::string name;
    std::string type;
    std::string description;
    std::function<void(const T &)> incomingMessageCallback;
    std::function<void(std::string_view)> transmitMessageCallback;
};

// this represents the message generator but not the transport layer.
template<class T>
class PubSub {
public:
    PubSub(const std::string &host,
           const int port, const std::string &topic) :
            m_io_context{},
            m_host(host),
            m_port(port),
            m_topic(topic)
    {}

private:
    std::string m_host;
    int m_port;
    std::string m_topic;
    boost::asio::io_context m_io_context;
};  // class PubSub
class BeastServer {
private:
    using TopicName = std::string;
    using TypeName = std::string;
    int m_port;
    std::string m_host;
    std::string m_topic;
    std::unordered_map<TopicName, std::function<void(json)>> m_subscribers;
    boost::asio::io_context m_io_context;
    //boost::asio::ip::tcp::acceptor m_acceptor;
public:
    BeastServer(const std::string &host,
                const int &port, const std::string &topic)
            : m_io_context(),
              m_host(host),
              m_port(port),
              m_topic(topic)
    //m_acceptor(m_io_context, boost::asio::ip::tcp::endpoint)
    {
    }

    void addSubscriber(TopicName topic, std::function<void(json)> callback)
    {
        m_subscribers[topic] = callback;
    }

    void start()
    {
//        m_acceptor{std::make_unique<boost::asio::ip::tcp::acceptor>(
//                m_io_context, boost::asio::ip::tcp::endpoint(
//                        boost::asio::ip::tcp::v4(), m_port))};
        //m_acceptor->listen();

        do_accept();
    }

    void do_accept()
    {
//        m_acceptor->async_accept(
//                [this](boost::system::error_code ec,
//                       boost::asio::ip::tcp::socket socket) {
//                    if (!ec) {
//                        std::make_shared<BeastClient>(std::move(socket))
//                                ->start();
//                    }
//                    do_accept();
//                });
    }
};

namespace beast = boost::beast;
class BeastClient {
private:
    boost::asio::ip::tcp::socket m_socket;
    beast::flat_buffer m_buffer;
    beast::http::request<beast::http::string_body> m_request;

    beast::http::response<beast::http::string_body> m_response;
    beast::http::request_parser<beast::http::string_body> m_parser;
    //beast::http::response_serializer<beast::http::string_body> m_serializer;
    // json serializer;
    nlohmann::json m_json;
public:
    BeastClient(boost::asio::ip::tcp::socket socket)
            : m_socket(std::move(socket))
    {
        socket.async_read_some(
                m_buffer,
                [this](beast::error_code ec, std::size_t bytes_transferred) {
                    if (!ec) {
                        //m_parser.put(m_buffer.data());
                        m_buffer.consume(bytes_transferred);
                        if (m_parser.is_done()) {
                            m_request = m_parser.release();
                            // handle the request
                            //handle_request();
                        } else {
                            //do_read();
                        }
                    }
                });
    }

    void handle_request()
    {
        // handle the request
        if (m_request.method() == beast::http::verb::get) {
            // handle the request
            if (m_request.target() == "/") {
                // handle the request
                m_response.result(beast::http::status::ok);
                m_response.set(beast::http::field::server, "Beast");
                m_response.set(beast::http::field::content_type,
                               "text/html");
                m_response.body() = "<html><body><h1>Hello, World!</h1></body></html>";
                m_response.prepare_payload();
                //do_write();
            } else if (m_request.target() == "/json") {
                // handle the request
                m_response.result(beast::http::status::ok);
                m_response.set(beast::http::field::server, "Beast");
                m_response.set(beast::http::field::content_type,
                               "application/json");
                m_response.body() =
                        R"({"message":"Hello, World!"})";

            }
        }
    }
};

class BeastJsonPubSub {
};

struct ConnectionInterface {
    std::function<void(std::string_view)> transmitCallback;
    std::function<void(std::string_view)> receiveCallback;
};

/**
 * use asio to create a server that can accept connections and send message.
 * we register a TransmitCallback and a call a receiveCallback when a message is received.
 */
class AsioSocketServer {
public:
    AsioSocketServer(/*std::shared_ptr<ConnectionInterface> connectionInterface,*/ int port)
            : m_connectionInterface(),
              m_io_context{},
              m_resolver(m_io_context),
              m_acceptor{m_io_context}// m_resolver.resolve("localhost", "0")},
              //boost::asio::ip::tcp::v4(), std::to_string(0)
    {
    }
    void start(){}

    void do_accept()
    {
/*        m_acceptor->async_accept(
                [this](boost::system::error_code ec,
                       boost::asio::ip::tcp::socket socket) {
                    if (!ec) {
                        std::make_shared<BeastClient>(std::move(socket))
                                //start();
                    }
                    do_accept();
                });*/
    }

    void transmitMessage(std::string_view str)
    {
        //m_connectionInterface->transmitCallback(str);
    }

private:
    boost::asio::ip::tcp::acceptor m_acceptor;
    ConnectionInterface m_connectionInterface;
    boost::asio::io_context m_io_context;
    boost::asio::ip::tcp::resolver m_resolver;
};
#endif
#endif // MIDDLEWARE_BEASTJSONPUBSUB_H
