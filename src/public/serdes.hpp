//
// Created by John Vishnefske on 6/15/2022.
//

#ifndef MIDDLEWARE_TEST_SERDES_HPP_
#define MIDDLEWARE_TEST_SERDES_HPP_


/**
 * provide load/dump interface for multiple type using a template json implementation.
 * @tparam variantTypes
 */
template<class... variantTypes>
class SerDes{
   public:
    constexpr SerDes(std::array<std::string_view, sizeof...(variantTypes)> typeNames, std::variant<variantTypes...>):
                                                                                                                            m_typeNames{typeNames}
    {
    }
    constexpr explicit SerDes(std::array<std::string_view,sizeof...(variantTypes)> typeNames): m_typeNames{typeNames}{

    }
    constexpr SerDes() = default;
    constexpr size_t indexOfIndex(int i){
        return m_typeArray.at(i).index();
    }
using MyVariant = std::variant<variantTypes...>;
    constexpr size_t indexOfLabel(const std::string_view typeLabel){
        const auto result = std::find(m_typeNames.begin(), m_typeNames.end(), typeLabel);
        if(result==m_typeNames.end()){
            throw std::runtime_error("invalid type label lookup");
        }
        // convert iterator to integer.
        const auto index = result - m_typeNames.begin();
        return index;
    }
    constexpr bool containsLabel(const std::string_view typeLabel){
        const auto iterator = std::find(m_typeNames.begin(), m_typeNames.end(), typeLabel) != m_typeNames.end();
        const bool is_found = (iterator != m_typeNames.end());
        return is_found;
    }
    constexpr MyVariant loads(const std::string_view typeLabel, const std::string_view message){
        // returns parsed document using a json which has a similar interface
        // to modern json.
        const auto index = indexOfLabel(typeLabel);
        const auto typeExample = m_typeArray.at(index);
        return std::visit([message](auto t){
            auto document = Json::parse(message);
            return MyVariant {document.get<decltype(t)>()};
        }, typeExample);
    }
    template<class Buffer>
    constexpr void dumps(const MyVariant v, Buffer & destination);
   private:
    template<class T>
    std::variant<variantTypes...> generatorImpl(T resultType){
        return T{};
    }
    std::variant<variantTypes...> generateAtIndex(int i){
        // this is much less general than std::variant::emplace<I>
        // but may be useful for collecting json conversion functions.
        return generatorImpl(m_typeArray.at(i));
    }

    //const std::variant<variantTypes...> m_container;
    std::array<std::string_view,sizeof...(variantTypes)> m_typeNames;
    static constexpr std::array<std::variant<variantTypes...>,sizeof...(variantTypes)> m_typeArray{variantTypes{}...};
    // now create an array of functions which return the specified type.
    static constexpr std::array<int, sizeof...(variantTypes)> m_generators{};
    //static constexpr std::array m_generatorArray{[]{return std::variant<variantTypes...>{ variantTypes{}}; }...} ;
};
#endif  // MIDDLEWARE_TEST_SERDES_HPP_
