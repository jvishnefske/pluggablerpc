//
// Created by John Vishnefske on 6/7/2022.
//
#include "public/ServiceDiscovery.h"

#include <string>
#include <functional>
#include <iostream>
#include <ostream>
#include <sstream>
//#include "dns_sd.h"
#if defined(WIN32) || defined(WIN64) || defined(__NT__)
#include <windows.h>
#include <windns.h>
//#include <windows.networking.servicediscovery.dnssd.h>

#elif __linux__
// add avahi support here. Constructor private until something is fixed.
class ServiceDiscovery{
    private:
    ServiceDiscovery() = default;
};
#else
// macos, or other dns-sd supported system.
#include "dns_sd.h"
void DefaultDnsCallback(DNSServiceRef,unsigned int, unsigned int,int,
                        const char*, const char*,const char*,void* p_context){
    std::cout  << "@" << __func__ << ":" << "DefaultDnsCallback" <<std::endl;
    if(p_context){
        std::cout << "context pointer appears non null."<<std::endl;
    }
}
void DNSServiceResolveCallback(
    DNSServiceRef sdRef,
    DNSServiceFlags /*flags*/,
    uint32_t /*interfaceIndex*/,
    DNSServiceErrorType /*errorCode*/,
    const char                          */*fullname*/,
    const char                          */*hosttarget*/,
    uint16_t /*port*/,                                   /* In network byte order */
    uint16_t /*txtLen*/,
    const unsigned char                 */*txtRecord*/,
    void                                *context
){

//    (DNSServiceRef p_handle, unsigned int, unsigned int, int,
//                               const char*, const char*, unsigned short,unsigned short,
//                               unsigned char *, void* p_context){
    std::cout  << "@" << __func__ << ":" << "DNSServiceResolveCallback" <<std::endl;
    if(context){
        std::cout << "context pointer appears non null."<<std::endl;
    }
    if(sdRef){
        std::cout << "context handle appears non null."<<std::endl;
    }
}
namespace ServiceDiscovery {
class Register {
    DNSServiceRef m_sdRef;
    DNSServiceFlags flags = kDNSServiceFlagsDefault;
    uint32_t interfaceIndex = 0; // zero is all interfaces
    static constexpr auto m_name = nullptr;  // use computer name if not provided
    // see http://www.dns-sd.org/ServiceTypes.html
    const std::string regType = "_ws._tcp";
    // auto register default domains if not provided.
    static constexpr auto m_domain = nullptr;
    // host must already exist or be created with DNSServiceRegisterRecord.
    static constexpr auto m_host = nullptr;
    int m_port;
    //std::array<TXTRecordRef ,1> m_txtRecord;
    static constexpr auto m_txtRecord = nullptr;
    static constexpr auto m_txtRecordLength = 0;
   public:
    Register(int port): m_port{port}{
        //const auto p_callback = std::mem_fn(&Register::callback);
        constexpr auto p_callback = nullptr;
        static constexpr auto context_ptr = nullptr;
        const auto error = DNSServiceRegister(&m_sdRef,flags,interfaceIndex, m_name, regType.c_str(),m_domain, m_host,
                           m_port, m_txtRecordLength, m_txtRecord, p_callback, context_ptr);
        if(error != kDNSServiceErr_NoError){
            std::ostringstream oss{};
            oss << "DNSServiceRegister error" << std::to_string(error);
            std::runtime_error(oss.str());
        }
    }
    void callback(
        DNSServiceRef /*sdRef*/, DNSServiceFlags /*flags*/, DNSServiceErrorType /*errorCode*/,
        const char */*name*/, const char */*regtype*/, const char */*domain*/, void */*context*/)
    {
        std::cout << "register callback" <<std::endl;
    }
    ~Register(){
        DNSServiceRefDeallocate(m_sdRef);
    }
};
class Browse {
   private:
    DNSServiceRef m_sdRef;
    DNSServiceFlags flags = kDNSServiceFlagsDefault; // unused
    const int m_interfaceIndex = 0;
    const std::string regType = "_ws._tcp";
    static constexpr auto domain = nullptr;
    static constexpr auto context = nullptr;
   public:

    Browse(){
        const auto error = DNSServiceBrowse(
            &m_sdRef, flags, m_interfaceIndex, regType.c_str(), domain, DefaultDnsCallback, context);
        if(error != kDNSServiceErr_NoError){
            std::ostringstream oss;
            oss << "DNSServiceBrowse error" << std::to_string(error);
            std::runtime_error(oss.str());
        }
    }
#if 0

    void callback(
        DNSServiceRef sdRef, DNSServiceFlags flags, uint32_t interfaceIndex,
        DNSServiceErrorType errorCode, const char *serviceName, const char *regtype,
        const char *replyDomain, void *context)
    {
        std::cout <<"browse callback"<<std::endl;
    }
#endif

    ~Browse(){
        DNSServiceRefDeallocate(m_sdRef);
    }
};
class Resolve{
   private:
    DNSServiceRef m_sdRef;
    DNSServiceFlags m_flags = kDNSServiceFlagsDefault; // unused
    uint32_t m_interfaceIndex = 0;
    std::string m_nameToResolve{};
    std::string m_serviceToResolve{};
    const std::string m_regType = "_ws._tcp";
    std::string m_domain{};
    static constexpr void* m_context = nullptr;
   public:
    Resolve(
        std::string nameToResolve, std::string serviceToResolve, std::string /*domain*/
        ):m_nameToResolve{nameToResolve},
         m_serviceToResolve{serviceToResolve}{
//        const auto p_callback = std::mem_fn(&Resolve::callback);
       /* DNSServiceErrorType error = */DNSServiceResolve(
            &m_sdRef, m_flags, m_interfaceIndex, m_serviceToResolve.c_str(),
            m_regType.c_str(), m_domain.c_str(), &DNSServiceResolveCallback, m_context
            );
    }
    void callback(
        DNSServiceRef /*sdRef*/, DNSServiceFlags /*flags*/, uint32_t /*interfaceIndex*/,
        DNSServiceErrorType /*errorCode*/, const char */*fullname*/, const char */*hosttarget*/,
        uint16_t /*port*/, uint16_t /*txtLen*/, const unsigned char */*txtRecord*/, void */*context*/)
    {
        std::cout <<"resolve callback" <<std::endl;
    }
    ~Resolve(){
        DNSServiceRefDeallocate(m_sdRef);
    }
}; // may be more appropriate as a functional interface.
} // namespace
#endif
