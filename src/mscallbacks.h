//
// Created by John Vishnefske on 11/22/21.
//

#ifndef MIDDLEWARE_MSCALLBACKS_H
#define MIDDLEWARE_MSCALLBACKS_H
#include <msquic.h>

#include <thread>

#include "queue.h"
// this is an example based on sample.c.
// It could be hooked up to a message queue, and thread pool, or whatever you
// want to do. The server's callback for stream events from MsQuic.
//

namespace {
// global context
static BlockingQueue<std::string>
    stateMessages;  // queue for sending messages to the Handler thread
}  // namespace
namespace middleware {
auto dequeue() -> std::string { return stateMessages.dequeue(); }
#if 0
    std::thread handlerThread{[]() {
        bool running = true;
        while (running) {
            auto msg = dequeue();
            if (msg == "stop") {
                running = false;
            }
            std::cout << msg << std::endl;
        }
    }};
#endif
}  // namespace middleware

/*typedef struct QUIC_CREDENTIAL_CONFIG_HELPER {
    QUIC_CREDENTIAL_CONFIG CredConfig;
    union {
        QUIC_CERTIFICATE_HASH CertHash;
        QUIC_CERTIFICATE_HASH_STORE CertHashStore;
        QUIC_CERTIFICATE_FILE CertFile;
        QUIC_CERTIFICATE_FILE_PROTECTED CertFileProtected;
    };
} QUIC_CREDENTIAL_CONFIG_HELPER;*/

extern "C" {
_IRQL_requires_max_(DISPATCH_LEVEL)
    _Function_class_(QUIC_STREAM_CALLBACK) QUIC_STATUS QUIC_API
    ServerStreamCallback(_In_ HQUIC Stream, _In_opt_ void* Context,
                         _Inout_ QUIC_STREAM_EVENT* Event) {
    // UNREFERENCED_PARAMETER(Context);
    switch (Event->Type) {
        case QUIC_STREAM_EVENT_SEND_COMPLETE:
            //
            // A previous StreamSend call has completed, and the context is
            // being returned back to the app.
            //
            free(Event->SEND_COMPLETE.ClientContext);
            printf("[strm][%p] Data sent\n", Stream);
            stateMessages.enqueue(
                "[strm][" + std::to_string(Event->SEND_COMPLETE.Canceled) +
                "] Data sent");
            break;
        case QUIC_STREAM_EVENT_RECEIVE:
            //
            // Data was received from the peer on the stream.
            //
            printf("[strm][%p] Data received\n", Stream);
            break;
        case QUIC_STREAM_EVENT_PEER_SEND_SHUTDOWN:
            //
            // The peer gracefully shut down its send direction of the stream.
            //
            printf("[strm][%p] Peer shut down\n", Stream);
            // ServerSend(Stream);
            break;
        case QUIC_STREAM_EVENT_PEER_SEND_ABORTED:
            //
            // The peer aborted its send direction of the stream.
            //
            printf("[strm][%p] Peer aborted\n", Stream);
            // MsQuic->StreamShutdown(Stream, QUIC_STREAM_SHUTDOWN_FLAG_ABORT,
            // 0);
            break;
        case QUIC_STREAM_EVENT_SHUTDOWN_COMPLETE:
            //
            // Both directions of the stream have been shut down and MsQuic is
            // done with the stream. It can now be safely cleaned up.
            //
            printf("[strm][%p] All done\n", Stream);
            // MsQuic->StreamClose(Stream);
            break;
        default:
            break;
    }
    return QUIC_STATUS_SUCCESS;
}

//
// The server's callback for connection events from MsQuic.
//
_IRQL_requires_max_(DISPATCH_LEVEL)
    _Function_class_(QUIC_CONNECTION_CALLBACK) QUIC_STATUS QUIC_API
    ServerConnectionCallback(_In_ HQUIC Connection, _In_opt_ void* Context,
                             _Inout_ QUIC_CONNECTION_EVENT* Event) {
    // UNREFERENCED_PARAMETER(Context);
    switch (Event->Type) {
        case QUIC_CONNECTION_EVENT_CONNECTED:
            //
            // The handshake has completed for the connection.
            //
            printf("[conn][%p] Connected\n", Connection);
            // MsQuic->ConnectionSendResumptionTicket(Connection,
            // QUIC_SEND_RESUMPTION_FLAG_NONE, 0, NULL);
            break;
        case QUIC_CONNECTION_EVENT_SHUTDOWN_INITIATED_BY_TRANSPORT:
            //
            // The connection has been shut down by the transport. Generally,
            // this is the expected way for the connection to shut down with
            // this protocol, since we let idle timeout kill the connection.
            //
            if (Event->SHUTDOWN_INITIATED_BY_TRANSPORT.Status ==
                QUIC_STATUS_CONNECTION_IDLE) {
                printf("[conn][%p] Successfully shut down on idle.\n",
                       Connection);
            } else {
                printf("[conn][%p] Shut down by transport, 0x%x\n", Connection,
                       Event->SHUTDOWN_INITIATED_BY_TRANSPORT.Status);
            }
            break;
        case QUIC_CONNECTION_EVENT_SHUTDOWN_INITIATED_BY_PEER:
            //
            // The connection was explicitly shut down by the peer.
            //
            printf("[conn][%p] Shut down by peer, 0x%llu\n", Connection,
                   (unsigned long long)
                       Event->SHUTDOWN_INITIATED_BY_PEER.ErrorCode);
            break;
        case QUIC_CONNECTION_EVENT_SHUTDOWN_COMPLETE:
            //
            // The connection has completed the shutdown process and is ready to
            // be safely cleaned up.
            //
            printf("[conn][%p] All done\n", Connection);
            // MsQuic->ConnectionClose(Connection);
            break;
        case QUIC_CONNECTION_EVENT_PEER_STREAM_STARTED:
            //
            // The peer has started/created a new stream. The app MUST set the
            // callback handler before returning.
            //
            printf("[strm][%p] Peer started\n",
                   Event->PEER_STREAM_STARTED.Stream);
            // MsQuic->SetCallbackHandler(Event->PEER_STREAM_STARTED.Stream,
            // (void*)ServerStreamCallback, NULL);
            break;
        case QUIC_CONNECTION_EVENT_RESUMED:
            //
            // The connection succeeded in doing a TLS resumption of a previous
            // connection's session.
            //
            printf("[conn][%p] Connection resumed!\n", Connection);
            break;
        default:
            break;
    }
    return QUIC_STATUS_SUCCESS;
}

//
// The server's callback for listener events from MsQuic.
//
_IRQL_requires_max_(PASSIVE_LEVEL)
    _Function_class_(QUIC_LISTENER_CALLBACK) QUIC_STATUS QUIC_API
    ServerListenerCallback(_In_ HQUIC Listener, _In_opt_ void* Context,
                           _Inout_ QUIC_LISTENER_EVENT* Event) {
    // UNREFERENCED_PARAMETER(Listener);
    // UNREFERENCED_PARAMETER(Context);
    QUIC_STATUS Status = QUIC_STATUS_NOT_SUPPORTED;
    switch (Event->Type) {
        case QUIC_LISTENER_EVENT_NEW_CONNECTION:
            //
            // A new connection is being attempted by a client. For the
            // handshake to proceed, the server must provide a configuration for
            // QUIC to use. The app MUST set the callback handler before
            // returning.
            //
            // MsQuic->SetCallbackHandler(Event->NEW_CONNECTION.Connection,
            // (void*)ServerConnectionCallback, NULL); Status =
            // MsQuic->ConnectionSetConfiguration(Event->NEW_CONNECTION.Connection,
            // Configuration);
            break;
        default:
            break;
    }
    return Status;
}

//
// The clients's callback for stream events from MsQuic.
//
_IRQL_requires_max_(DISPATCH_LEVEL)
    _Function_class_(QUIC_STREAM_CALLBACK) QUIC_STATUS QUIC_API
    ClientStreamCallback(_In_ HQUIC Stream, _In_opt_ void* Context,
                         _Inout_ QUIC_STREAM_EVENT* Event) {
    // UNREFERENCED_PARAMETER(Context);
    switch (Event->Type) {
        case QUIC_STREAM_EVENT_SEND_COMPLETE:
            //
            // A previous StreamSend call has completed, and the context is
            // being returned back to the app.
            //
            free(Event->SEND_COMPLETE.ClientContext);
            printf("[strm][%p] Data sent\n", Stream);
            break;
        case QUIC_STREAM_EVENT_RECEIVE:
            //
            // Data was received from the peer on the stream.
            //
            printf("[strm][%p] Data received\n", Stream);
            break;
        case QUIC_STREAM_EVENT_PEER_SEND_ABORTED:
            //
            // The peer gracefully shut down its send direction of the stream.
            //
            printf("[strm][%p] Peer aborted\n", Stream);
            break;
        case QUIC_STREAM_EVENT_PEER_SEND_SHUTDOWN:
            //
            // The peer aborted its send direction of the stream.
            //
            printf("[strm][%p] Peer shut down\n", Stream);
            break;
        case QUIC_STREAM_EVENT_SHUTDOWN_COMPLETE:
            //
            // Both directions of the stream have been shut down and MsQuic is
            // done with the stream. It can now be safely cleaned up.
            //
            printf("[strm][%p] All done\n", Stream);
            // MsQuic->StreamClose(Stream);
            break;
        default:
            break;
    }
    return QUIC_STATUS_SUCCESS;
}

//
// The clients's callback for connection events from MsQuic.
//
_IRQL_requires_max_(DISPATCH_LEVEL)
    _Function_class_(QUIC_CONNECTION_CALLBACK) QUIC_STATUS QUIC_API
    ClientConnectionCallback(_In_ HQUIC Connection, _In_opt_ void* Context,
                             _Inout_ QUIC_CONNECTION_EVENT* Event) {
    // UNREFERENCED_PARAMETER(Context);
    switch (Event->Type) {
        case QUIC_CONNECTION_EVENT_CONNECTED:
            //
            // The handshake has completed for the connection.
            //
            printf("[conn][%p] Connected\n", Connection);
            // todo ClientSend(Connection);
            break;
        case QUIC_CONNECTION_EVENT_SHUTDOWN_INITIATED_BY_TRANSPORT:
            //
            // The connection has been shut down by the transport. Generally,
            // this is the expected way for the connection to shut down with
            // this protocol, since we let idle timeout kill the connection.
            //
            if (Event->SHUTDOWN_INITIATED_BY_TRANSPORT.Status ==
                QUIC_STATUS_CONNECTION_IDLE) {
                printf("[conn][%p] Successfully shut down on idle.\n",
                       Connection);
            } else {
                printf("[conn][%p] Shut down by transport, 0x%x\n", Connection,
                       Event->SHUTDOWN_INITIATED_BY_TRANSPORT.Status);
            }
            break;
        case QUIC_CONNECTION_EVENT_SHUTDOWN_INITIATED_BY_PEER:
            //
            // The connection was explicitly shut down by the peer.
            //
            printf("[conn][%p] Shut down by peer, 0x%llu\n", Connection,
                   (unsigned long long)
                       Event->SHUTDOWN_INITIATED_BY_PEER.ErrorCode);
            break;
        case QUIC_CONNECTION_EVENT_SHUTDOWN_COMPLETE:
            //
            // The connection has completed the shutdown process and is ready to
            // be safely cleaned up.
            //
            printf("[conn][%p] All done\n", Connection);
            if (!Event->SHUTDOWN_COMPLETE.AppCloseInProgress) {
                // todo MsQuic->ConnectionClose(Connection);
            }
            break;
        case QUIC_CONNECTION_EVENT_RESUMPTION_TICKET_RECEIVED:
            //
            // A resumption ticket (also called New Session Ticket or NST) was
            // received from the server.
            //
            printf("[conn][%p] Resumption ticket received (%u bytes):\n",
                   Connection,
                   Event->RESUMPTION_TICKET_RECEIVED.ResumptionTicketLength);
            for (uint32_t i = 0;
                 i < Event->RESUMPTION_TICKET_RECEIVED.ResumptionTicketLength;
                 i++) {
                printf("%.2X", (uint8_t)Event->RESUMPTION_TICKET_RECEIVED
                                   .ResumptionTicket[i]);
            }
            printf("\n");
            break;
        default:
            break;
    }
    return QUIC_STATUS_SUCCESS;
}
};
#endif  // MIDDLEWARE_MSCALLBACKS_H
