//
// Created by John Vishnefske on 7/27/2022.
//

#ifndef MIDDLEWARE_SRC_ASTUPLE_USINGPFR_H_
#define MIDDLEWARE_SRC_ASTUPLE_USINGPFR_H_

#include <boost/beast.hpp>
#include <boost/describe.hpp>
#include <any>
#include <iostream>

#include <boost/json.hpp>
#include <boost/throw_exception.hpp>
#include <boost/type_index.hpp>
#include <boost/pfr.hpp>


namespace middleware { namespace descriptorless {

std::string struct_to_string(auto obj) {
    const auto typeName =
        boost::typeindex::type_id<decltype(obj)>().pretty_name();
    boost::json::array members;
    boost::pfr::for_each_field(
        obj, [&](auto member) { members.emplace_back(member); });
    boost::json::object result{{typeName, members}};
    return boost::json::serialize(result);
}

template <class T>
struct PiecewiseConstruct {
    using ResultType = T;
    explicit PiecewiseConstruct(boost::json::array members)
    {
        m_memberValues = members;
        m_cursor = m_memberValues.begin();
    }

    template <class... MemberType>
    ResultType operator()(MemberType...) {
        return T{pop_value_as<MemberType>()...};
    }
    template <class T2>
    T2 pop_value_as();

   private:
    boost::json::array m_memberValues;
    boost::json::array::iterator m_cursor;
};

template <class T>
template <class T2>
T2 PiecewiseConstruct<T>::pop_value_as() {
    if (m_cursor == m_memberValues.end()) {
        BOOST_THROW_EXCEPTION(std::runtime_error("aaaaaa!!!!!"));
    }
    auto result = boost::json::value_to<T2>(*m_cursor);
    m_cursor++;
    return result;
}

template <class T>
T string_to_struct(std::string str) {
    boost::json::value jsonObj = boost::json::parse(str);

    const auto iterator = jsonObj.as_object().begin();
    BOOST_ASSERT(iterator != jsonObj.as_object().end());
    boost::string_view typeName = iterator->key();
    boost::json::array members = iterator->value().as_array();
    std::cout << "string_to_struct: " << members << std::endl;
    BOOST_ASSERT(typeName == boost::typeindex::type_id<T>().pretty_name());
    PiecewiseConstruct<T> pc(members);
    T empty;
    return std::apply(pc, boost::pfr::structure_to_tuple(empty));
}


// in order to eliminate json library as a dependancy of the library users
// we convert the json to a key string, a vector of std::variant?

}} // namespace

#endif  // MIDDLEWARE_SRC_ASTUPLE_USINGPFR_H_
