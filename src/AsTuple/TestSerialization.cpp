//
// Created by John Vishnefske on 7/27/2022.
//
#include <boost/assert.hpp>
#include <boost/json.hpp>
#include <boost/pfr.hpp>
#include <boost/test/unit_test.hpp>
#include <boost/throw_exception.hpp>
#include <boost/type_index.hpp>
#include "UsingPFR.h"

namespace {
struct Example1 {
    int a;
    double b;
    std::string c;
};

using namespace middleware::descriptorless;
}  // namespace

BOOST_AUTO_TEST_CASE(CustomToJson) {
    Example1 expected = {1, 2, "hello"};
    auto buffer = struct_to_string(expected);
    std::cout << "expected = " << buffer << std::endl;
    BOOST_CHECK(
        R"({"struct `anonymous namespace'::Example1":[1,2E0,"hello"]})" ==
        buffer);
    auto result = string_to_struct<Example1>(buffer);

    BOOST_CHECK(boost::pfr::eq(expected, result));
    auto string2 = struct_to_string(result);
    std::cout << "result = " << string2 << std::endl;
    BOOST_CHECK(string2 == buffer);
}