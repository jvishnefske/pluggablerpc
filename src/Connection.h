/**
 * @file   Connection.h
 * @author  John Vishnefske <jvishnefske@acm.org>
 * @date    11/19/21
 * @brief   Provides a simple interface to the http libraries.
 * Allows for pubsub, command result, and request/response.
 * future consideration include unreliable connections for low latency realtime
 * requirements. This allows exploring the performance of RFC9000 QUIC in
 * various scenarios.
 */

// todo make a client, and server connection ☹.
#ifndef MIDDLEWARE_CONNECTION_H
#define MIDDLEWARE_CONNECTION_H

// todo: split into cpp implmentation for library comipile time improvement.

#include <array>
#include <future>
#include <memory>
#include <nlohmann/json.hpp>
#include <variant>
//#include <msquic.h>
#include <functional>
#include <iostream>
#include <map>
#include <mutex>
#include <string>
#include <vector>

#include "mscallbacks.h"
extern "C" {
//    QUIC_LISTENER_EVENT
//    static QUIC_STATUS ServerHandlerCallback(HQUIC Listener,
//    QUIC_LISTENER_EVENT *event)
//    {
//        std::cout << "ServerHandlerCallback" << std::endl;
//        return QUIC_STATUS_SUCCESS;
//    }
//    QUIC_STREAM_EVENT
//    static QUIC_STATUS StreamHandlerCallback(HQUIC Stream, QUIC_STREAM_EVENT
//    *event){
//        std::cout << "StreamHandlerCallback" << std::endl;
//        return QUIC_STATUS_SUCCESS;
//    }
//    //    QUIC_CONNECTION_EVENT
//    static QUIC_STATUS ConnectionHandlerCallback(HQUIC Connection,
//    QUIC_CONNECTION_EVENT *event){
//        std::cout << "ConnectionHandlerCallback" << std::endl;
//        return QUIC_STATUS_SUCCESS;
//    }
}  // extern "C"

namespace middleware {
using namespace std::placeholders;

template <class StreamFactory>
class Publisher;

class QuicStream;

class QuicDatagram;

using PtrQuicDatagram = std::shared_ptr<QuicDatagram>;
using PtrQuicStream = std::shared_ptr<QuicStream>;
using BinaryData = std::vector<char>;

// can be shared by two classes one of which sends, and one of which pushes
// responses. This is relevant for both server, and client libraries, and also
// Server
class SingleStream {
   public:
    std::function<void(BinaryData)> ReceiveCallback;
    std::function<void(BinaryData)> SendCallback;
};

enum Qos {
    qLowPriority  // low priority stream
};
enum Reliability {
    qReliable,  // use streams
    // qBestEffort;   // use datagram
};
enum Durability {
    // qTransientLocal; // keeps track of last sample for late subscribers
    qVolatile  // no persistance of samples.
};

class JsonHandler {
   private:
    // map topics to callbacks
    std::map<std::string, std::function<void(nlohmann::json)>> subscribers;

   public:
    void operator()(BinaryData data) {
        // parse binary data into json
        nlohmann::json j = nlohmann::json::parse(data.data());
        std::cout << "JsonHandler: " << data.data() << std::endl;
    }

    void addSubscriber(std::string topic,
                       std::function<void(nlohmann::json)> callback) {
        subscribers[topic] = callback;
    }

    void removeSubscriber(std::string topic) { subscribers.erase(topic); }
};

template <class Handler, class ServerImpl>
class ServerInterface {
   private:
   public:
    void bind(int port, std::string url);

    void setConnectionHandler(std::function<SingleStream>);
};

/**
 *  quic raii warpper to c api pointer
 */
class MsQuicRegistration {
   private:
    QUIC_REGISTRATION_CONFIG m_config = {
        .AppName = "middleware",
        .ExecutionProfile = QUIC_EXECUTION_PROFILE_LOW_LATENCY};

   public:
    const QUIC_API_TABLE *api;
    QUIC_HANDLE *m_registration;
    QUIC_HANDLE *m_listener = nullptr;
    MsQuicRegistration() {
        const QUIC_API_TABLE *localApi;
        auto status = MsQuicOpen(&localApi);
        api = localApi;
        if (QUIC_FAILED(status)) {
            std::cout << "MsQuicOpen failed: " << status << std::endl;
            std::runtime_error("MsQuicOpen failed");
        }

        // outputs m_registration, so we can only have one.
        status = api->RegistrationOpen(&m_config, &m_registration);
        if (QUIC_FAILED(status)) {
            throw std::runtime_error("MsQuicOpenRegistration failed ");
        }
    }
    ~MsQuicRegistration() {
        if (m_listener) {
            api->ListenerClose(m_listener);
        }
        api->RegistrationClose(m_registration);
        MsQuicClose(api);
    }
    /*
     *
     */
    QUIC_HANDLE *GetListenerHandle() {
        if (m_listener == nullptr) {
            auto status = api->ListenerOpen(
                m_registration, ServerListenerCallback, nullptr, &m_listener);
            if (QUIC_FAILED(status)) {
                std::cout << "ListenerOpen failed: " << status << std::endl;
                std::runtime_error("ListenerOpen failed");
            }
        }
        return m_listener;
    }
};

class MsQuicServer {
   private:
    MsQuicRegistration m_quic;
    decltype(SingleStream::ReceiveCallback) m_connectionHandler;
    HQUIC m_listener;
    std::array<uint8_t, 1024> m_array;
    const QUIC_BUFFER m_buffer = {
        .Length = static_cast<uint32_t>(m_array.size()),
        .Buffer = m_array.data()};

   public:
    MsQuicServer(int port = 8000, std::string url = "0.0.0.0") {
        // todo set configuration.
        QUIC_ADDR Address = {0};
        Address.Ipv4.sin_port = htons(port);
        Address.Ipv4.sin_family = AF_INET;
        Address.Ipv4.sin_addr.s_addr = inet_addr(url.c_str());
        m_listener = m_quic.GetListenerHandle();
    }
    void setConnectionHandler(
        const decltype(SingleStream::ReceiveCallback) connectionHandler) {
        m_connectionHandler = connectionHandler;
    }
};
using MsQuicError = std::runtime_error;

// using QuicServer = QuicServerImpl<JsonHandler>;

class MsQuicClient {
   private:
    MsQuicRegistration m_quic;

   public:
    MsQuicClient(int port = 8000, std::string url = "localhost") {
        std::cout << "MsQuicClient" << url << ":" << port << std::endl;
    }
};
}  // namespace middleware
#endif  // MIDDLEWARE_CONNECTION_H
