//
// Created by John Vishnefske on 7/26/2022.
//

#ifndef MIDDLEWARE_SRC_TYPEDESCRIPTOR_BOOSTDESCRIBED_H_
#define MIDDLEWARE_SRC_TYPEDESCRIPTOR_BOOSTDESCRIBED_H_


/**
 * examples for convinience ...
 */

namespace vsi {
struct FirstExample {int a,b;};
struct SecondExample {float c,d;};




template <class... Messages>
class BoostDescribed {};
} // namespace vsi
#endif  // MIDDLEWARE_SRC_TYPEDESCRIPTOR_BOOSTDESCRIBED_H_
