//
// Created by John Vishnefske on 7/30/22.
//

#ifndef MIDDLEWARE_MESSAGEROUTER_H
#define MIDDLEWARE_MESSAGEROUTER_H
#include <any>
#include <boost/asio/coroutine.hpp>
#include <boost/asio/ts/net.hpp>
#include <boost/beast/core/error.hpp>
#include <iosfwd>
#include <string>
#include <unordered_map>
#include <vector>

// forware declaration
class MessageStreamDealer;

/**
 * listner containes a router so that the router can accept onMessage from
 * Dealers.
 */
namespace middleware {
namespace websocket {
namespace net = boost::asio;
enum class Operations { publish, subscribe, announce };
struct GenericMessage {
    Operations op;
    std::string topic;
    std::any messageContainer;
};
using MessageWriter = std::function<void(GenericMessage)>;
class MessageRouter : public std::enable_shared_from_this<MessageRouter>,
                      public boost::asio::coroutine {
   public:
    MessageRouter() = default;
    void onMessage(GenericMessage m, MessageWriter writable);

   private:
    std::unordered_map<std::string, std::vector<MessageWriter>> m_subscribers;
};
}  // namespace websocket
}  // namespace middleware
#endif  // MIDDLEWARE_MESSAGEROUTER_H
