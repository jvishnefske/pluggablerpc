//
// Created by John Vishnefske on 7/30/22.
//

#ifndef MIDDLEWARE_MESSAGESTREAMDEALER_H
#define MIDDLEWARE_MESSAGESTREAMDEALER_H
#include <boost/asio/coroutine.hpp>
#include <boost/asio/ts/net.hpp>
#include <memory>

#include "MessageStreamDealer.h"
namespace middleware {
namespace websocket {
class MessageStreamDealer
    : public std::enable_shared_from_this<MessageStreamDealer>,
      public boost::asio::coroutine {
   public:
    MessageStreamDealer(boost::asio::ip::tcp::socket &&socket)
        : m_socket(std::move(socket)) {}
    void run() { BOOST_THROW_EXCEPTION(std::runtime_error("not implemented")); }

   private:
    boost::asio::ip::tcp::socket m_socket;
};

class Publisher {};
template <class UserMessage_t>
struct MessageSubscriptionCallback {
    using SubscriptionCallback = std::function<void(UserMessage_t)>;
    SubscriptionCallback callback;
};
}  // namespace websocket
}  // namespace middleware
#endif  // MIDDLEWARE_MESSAGESTREAMDEALER_H
