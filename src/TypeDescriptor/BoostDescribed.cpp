//
// Created by John Vishnefske on 7/26/2022.
//

#include "BoostDescribed.h"

#include <boost/describe.hpp>
#include <boost/json.hpp>
#include <boost/mp11.hpp>
#include <boost/mp11/mpl_list.hpp>
#include <boost/test/unit_test.hpp>
#include <boost/type_index.hpp>
#include <string_view>

namespace vsi {
BOOST_DESCRIBE_STRUCT(FirstExample, (), (a, b));
BOOST_DESCRIBE_STRUCT(SecondExample, (), (c, d));
/**
 * take a list of described classes, and create serdes for all
 */

template <class... D>
auto struct_to_string_tuple(D... d) {}

namespace desc = boost::describe;

template <class T, template <class...> class L, class... D>
auto struct_to_string_impl(T const& t, L<D...>) {
    boost::json::object o;
    (o.emplace(D::name, t.*D::pointer), ...);
    return boost::json::serialize(o);
    //    boost::mp11::mp_apply<struct_to_string_tuple, L>(l);
    //    return (std::make_tuple(D::name), ...);
    //    std::string ss;
    //    (ss.append(D::name),...);
    //    return ss;
    // return std::make_tuple( t.*D::pointer... );
}
template <class T, template <class...> class L, class... D>
auto struct_to_tuple_impl(T const& t, L<D...>) {
    // boost::json::object o;
    // (o.emplace(D::name, t.*D::pointer), ...);
    // return boost::json::serialize(o);
    //    boost::mp11::mp_apply<struct_to_string_tuple, L>(l);
    //    return (std::make_tuple(D::name), ...);
    return std::make_tuple(t.*D::pointer...);
}

template <class T,
          class Dm =
              desc::describe_members<T, desc::mod_public | desc::mod_inherited>,
          class En = std::enable_if_t<!std::is_union<T>::value> >
auto struct_to_string(T const& t) {
    return struct_to_string_impl(t, Dm());
}

template <class T,
          class Dm =
              desc::describe_members<T, desc::mod_public | desc::mod_inherited>,
          class En = std::enable_if_t<!std::is_union<T>::value> >
auto struct_to_tuple(T const& t) {
    return struct_to_tuple_impl(t, Dm());
}

template <class T, template <class...> class L, class... D>
auto string_to_struct_impl(std::string buffer, L<D...>) {
    boost::json::object o{boost::json::parse(buffer).as_object()};
    T t;
    ((t.*D::pointer = o.at(D::name)), ...);
    return t;
}

template <class T,
          class Dm =
              desc::describe_members<T, desc::mod_public | desc::mod_inherited>,
          class En = std::enable_if_t<!std::is_union<T>::value> >
T constexpr string_to_struct(std::string_view buffer) {
    string_to_struct_impl(buffer, Dm());
}

template <class T>
std::string to_string(const T& /*described*/) {
    boost::json::object o;
    // o.at( boost::mp11::mp_first<T>.name) = *boost::mp11::mp_first<T>.pointer;
    o.at("b") = 590796097;
    return boost::json::serialize(o);
}
// template<class T> void extract( boost::json::object const & obj, char const *
// name, T & value )
//{
//     value = boost::json::value_to<T>( obj.at( name ) );
// }

// template <class T,
//           class D1 = boost::describe::describe_members<
//               T, boost::describe::mod_public |
//               boost::describe::mod_protected>,
//           class D2 = boost::describe::describe_members<
//               T, boost::describe::mod_private>,
//           class En = std::enable_if_t<boost::mp11::mp_empty<D2>::value &&
//                                       !std::is_union<T>::value>>
// void tag_invoke(boost::json::value_to_tag<T> const&, boost::json::value
// const& v) {
//     (void)v;
//     T t{};
//     boost::mp11::mp_for_each <D1>([&t](auto D){
//         extract(obj, D.name, t.*D.pointer);
//     });
//     std::string_view f;
//     std::tuple{D1};
// }

// template <class... T>
// std::tuple<T...> extract_all(boost::json::object& obj){
//     //std::cout << "Extract" ((<< T.name)...)<<std::endl;
//     //(static_assert(std::is_pointer_v<decltype(T.pointer)>, "T is not a
//     pointer" )...); return std::tuple{
//     boost::json::value_to<std::remove_pointer<(T.pointer)>>(obj.at(T.name))...};
//
// }

auto extract_all(auto described) {
    static_assert(
        boost::describe::has_describe_members<decltype(described)>::value &&
            !std::is_union<decltype(described)>::value,
        "requires reflection description");
    using D1 = boost::describe::describe_members<decltype(described),
                                                 boost::describe::mod_public>;
}
template <typename... D1>
void print_names(D1... members) {
    ((std::cout << ":" << members.name), ...);
}

// template <class T>
// T from_string(const std::string_view buffer) {
//
//     const auto bsv = boost::string_view{buffer.data(), buffer.size()};
//     boost::json::value v{boost::json::parse(bsv)};
//     return boost::json::value_to<T>(v);
// }
}  // namespace vsi

BOOST_AUTO_TEST_CASE(boost_describe) {
    vsi::FirstExample expected{1, 2};
    BOOST_CHECK(1 == expected.a);
    BOOST_CHECK(2 == expected.b);
    const auto buffer{vsi::struct_to_tuple(expected)};

    //    BOOST_CHECK("a" ==std::get<0>(buffer));
    //    BOOST_CHECK("b" ==std::get<1>(buffer));
    BOOST_CHECK(1 == std::get<0>(buffer));
    BOOST_CHECK(2 == std::get<1>(buffer));
    const auto sResult = vsi::struct_to_string(expected);
    BOOST_CHECK(sResult == "{\"a\":1,\"b\":2}");
    //    const auto result{vsi::from_string<vsi::FirstExample>(buffer)};
    //    BOOST_CHECK(expected.a == result.a);
}

BOOST_AUTO_TEST_CASE(DumpDescribed) {
    vsi::FirstExample e{3, 4};
    boost::json::object valueobj;
    valueobj["a"] = 5;
    valueobj["b"] = 6;
    using D = boost::describe::describe_members<vsi::FirstExample,
                                                boost::describe::mod_public>;
    std::cout << "DumpDescribed" << boost::json::serialize(valueobj)
              << std::endl;
    //    boost::mp11::mp_apply<vsi::extract_all, D>(obj);
    std::tuple t{boost::mp11::mp_for_each<D>([](auto d) { return d.name; })};
    // static_assert(std::tuple_size<decltype(t)>::value == 2, "unexpected
    // member count");
    std::cout << "member type name is: "
              << boost::typeindex::type_id<decltype(t)>().pretty_name()
              << std::endl;
    BOOST_CHECK(2 == boost::mp11::mp_size<D>::value);
    // std::cout << std::get<0>(t) <<" " << std::get<1>(t) << std::endl;
}