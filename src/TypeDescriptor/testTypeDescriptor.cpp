//
// Created by John Vishnefske on 7/20/2022.
//
#include <boost/asio.hpp>
#include <boost/describe.hpp>
#include <boost/json.hpp>
#include <boost/pfr.hpp>
#include <boost/test/unit_test.hpp>
#include <boost/utility/string_view.hpp>
#include <boost/variant2.hpp>

#include "typedescriptors.h"

namespace middleware {
using namespace boost;
using namespace boost::describe;
struct Thing1 {
    int x{};
    double y{};
};
template <>
struct TypeDescriptor<Thing1> : MemberList<&Thing1::x, &Thing1::y> {};

enum class ExampleStates {
    kRunning,
    kStopped,
};
BOOST_DESCRIBE_ENUM(ExampleStates, kRunning, kStopped);

struct Thing2 {
    uint64_t a{};
    float z{};
    // ExampleStates currentState{};
};
BOOST_DESCRIBE_STRUCT(Thing2, (), (a, z));

// boost::describe::
boost::json::object gen_publish_message(const std::string topic,
                                        const boost::json::value val) {
    boost::json::object target_obj;
    target_obj.insert({{"op", "publish"}, {"topic", topic}, {"value", val}});
    //    target_obj.insert_or_assign("op","publish");
    //    target_obj.insert_or_assign("topic", topic);
    //    target_obj.insert_or_assign("value", val);
    return target_obj;
}

template <class T>
std::string pfr_dumps(T obj) {
    //    boost::pfr::for_each_field(){
    const auto item_tuple = boost::pfr::structure_to_tuple(obj);
    boost::json::value doc;
    boost::json::value_from(item_tuple, doc);
    boost::json::object value_obj{{"_serialization_scheme","tuple"},{"_serialization_scheme_tuple",doc}};
    return boost::json::serialize(gen_publish_message("testTopic", value_obj));
}
template <class T, class Bd = describe_bases<T, mod_any_access>,
          class Md = describe_members<T, mod_any_access>,
          class En = std::enable_if_t<!std::is_union<T>::value>>
std::string describe_dumps(T const obj) {
    boost::json::object target_obj;
    boost::mp11::mp_for_each<Md>(
        [&](auto D) { target_obj.insert_or_assign(D.name, obj.*D.pointer); });
    target_obj.insert_or_assign("_serialization_scheme", "dict");
    return boost::json::serialize(gen_publish_message("topic1", target_obj));
}
/**
 * this could be incorporated with a message parser that understands all
 * topics, and creates the correct variant instance based on the topic name.
 *
 * @tparam T
 * @tparam En
 * @param buffer
 * @return
 */
template <class T,
          class En = std::enable_if_t<std::is_default_constructible_v<T>>>
T loads_base(string_view buffer) {
    //    static T empty{};
    static constexpr auto empty_tuple{boost::pfr::structure_to_tuple(T{})};
    static boost::json::parser my_parser{};
    static std::mutex parser_mutex;
    boost::json::value my_value;
    {
        std::unique_lock<std::mutex> parser_lock{parser_mutex};
        my_parser.reset();
        my_parser.write(buffer);
        auto result{empty_tuple};
        my_value = my_parser.release();
    }
    auto op = my_value.at("op");
    BOOST_ASSERT("publish" == op);
    auto value_json = my_value.at("value");
    const auto serialization_scheme = my_value.at("_serialization_scheme");
    if (serialization_scheme == "typle") {
        const auto value_array =
            value_json.at("_serialization_scheme_typle").as_array();
        BOOST_ASSERT(sizeof empty_tuple == value_array.size());
        auto array_iterator = value_array.begin();
        auto result_tuple = mp11::tuple_apply(
            [&](auto attr) {
                const boost::json::value v = *(array_iterator++);
                return json::value_to<decltype(attr)>(v);
            }, std::make_tuple(empty_tuple)
            );
        std::tuple<uint64_t, float> dummy_example{};
        return mp11::construct_from_tuple<T>(dummy_example);
//        return mp11::construct_from_tuple<T>(result_tuple);

    } else if (serialization_scheme == "dict") {
        // get items out of value_json
        using Md = describe_members<T, mod_any_access>;
        T obj;
        boost::mp11::mp_for_each<Md>(
            //json::value_to<decltype(obj.*D.pointer)>

            [&](auto D) { obj.*D.pointer = boost::json::value_to<std::remove_reference_t<decltype(obj.*D.pointer)>>( value_json.at(D.name)); } );
        return obj;
    } else {
        return {};
    }
}
} // namespace middleware
BOOST_AUTO_TEST_CASE(ReflectionSerDes) {
    middleware::Thing1 thing1{1, 2.0};
    middleware::Thing2 thing2{3, 4.0};
    const auto str1 = pfr_dumps(thing1);
    BOOST_TEST(
        "{\"op\":\"publish\",\"topic\":\"testTopic\",\"value\":{\"_serialization_scheme\":\"tuple\",\"_serialization_scheme_tuple\":[1,2E0]}}" ==
        str1);
    const auto str2 = describe_dumps(thing2);
    BOOST_TEST(
        "{\"op\":\"publish\",\"topic\":\"topic1\",\"value\":{\"a\":3,\"z\":4E0,\"_serialization_scheme\":\"dict\"}"
        "}" == str2);
    const auto describe_result = middleware::loads_base<middleware::Thing2>(str2);
    BOOST_TEST(boost::pfr::eq(thing2, describe_result));
//    const auto pfr_result = middleware::loads_base<middleware::Thing1>(str1);
//    BOOST_TEST(boost::pfr::eq(thing2, pfr_result));

}

BOOST_AUTO_TEST_CASE(TypeDescriptorTest1,
                     *boost::unit_test::tolerance(0.00001)) {
    middleware::Thing1 thing1;
    BOOST_TEST(thing1.x == 0);
    BOOST_TEST(thing1.y == 0);
    BOOST_TEST(1.0 == 1.000001);
    // consider exercising the apply method.
}

// for some weird readson bishop fox things that 1E400 should be parsable and
// not infinite.
BOOST_AUTO_TEST_CASE(boostJsonTest, *boost::unit_test::expected_failures(1)) {
    std::string experiment =
        "{\"description\": \"Float (exp)\", \"test\": 1E400}";
    boost::json::value test = boost::json::parse(experiment);
    std::string real = boost::json::serialize(test);
    BOOST_TEST(experiment == real);
}

/*
 * this is a specific implementaion between type descriptors and byte stream.
 */
template <class Buffer, Described T>
void serialize_boost_json(Buffer& /*buffer*/, const T& obj) {
    boost::json::array a;
    return TypeDescriptor<T>::apply([&a](auto const&... members) {
        // build up a json string one member at a time.
        (a.push_back(members),...);
    }, obj);
//    buffer = boost::json::serialize(a);
}

template <class Buffer, Described T>
void deserialize_boost_json(Buffer& /*buffer*/, T& /*obj*/) {
    // the described class includes enough information to create a tuple, but
    // does not store the names as runtime strings for descriptive javascript.

}

BOOST_AUTO_TEST_CASE(BoostJsonTest) {
    middleware::Thing1 expected{1, 2};
    middleware::Thing1 result;
    std::string representation;
    serialize_boost_json<std::string, middleware::Thing1>(representation, expected);
    deserialize_boost_json(representation, result);
    BOOST_CHECK(expected.x == result.x);
    BOOST_CHECK(expected.y == result.y);
}

/**
 *  descrivbe a type including tags
 * @tparam Archive
 * @tparam Buffer
 */
template <class Archive>
void description(Archive& ar, middleware::Thing1& obj, int /*version*/) {
    ar.describe("x", obj.x);
    ar.describe("y", obj.y);
}

/**
 * build up a string
 */
class JsonPacker {
    /**
     * set a cursor to a specific string
     */
   public:
     template <class T,
         typename = std::enable_if_t<std::is_convertible_v<T, boost::json::value>>>
     void describe(std::string name, T &value){
        m_value[name] = value;
     }
     std::string string(){
         const auto result  = boost::json::value(m_value);
         return boost::json::serialize(m_value);
     }
   private:
    boost::json::object m_value{};
};
/**
 * parse a string and allow retriving based on tags
 */
template<class T>
void extractFromJson( boost::json::object const& obj, T& t, boost::string_view key )
{
//    auto boost_key = boost::string_view(key);
    t = boost::json::value_to<T>( obj.at( key ) );
}
class JsonUnpacker {
   public:
    JsonUnpacker(std::string source) : m_value{boost::json::parse(source).get_object()} {}
    template <class T>
    void describe(std::string name, T &value){
        extractFromJson(m_value, value, name);
    }
   private:
    boost::json::object m_value;
};
/**
 * get passed to a type description in order to serdes between json, and
 * described types.
 */
class JsonArchive {
   public:
    template <class T>
    void serialize(T & obj, std::string & result, int version=1) {
        JsonPacker p;
        description(p, obj, version);
        result = p.string();
    }
    template <class T>
    void deserialize( std::string input, T& obj, int version=1) {
        JsonUnpacker a{std::move(input)};
        description(a, obj, version);
    }
};
BOOST_AUTO_TEST_CASE(PossibeArchiveStory) {
    // concept for user facing api design.
    JsonArchive a;
    middleware::Thing1 initial{1,2};
    middleware::Thing1 result{};
    std::string repr;
    a.serialize(initial, repr);
    BOOST_CHECK("{\"x\":1,\"y\":2}" == repr);
    a.deserialize(repr, result);
    BOOST_CHECK(initial.x == result.x);
    BOOST_CHECK(initial.y == result.y);
}
