//
// Created by John Vishnefske on 7/31/22.
//
#ifndef MIDDLEWARE_LISTENER_HPP
#define MIDDLEWARE_LISTENER_HPP
#include <boost/asio/coroutine.hpp>
#include <boost/asio/ts/net.hpp>
#include <boost/beast/core/error.hpp>

namespace middleware {
namespace websocket {
namespace net = boost::asio;
class Listener : public std::enable_shared_from_this<Listener>,
                 public boost::asio::coroutine {
   public:
    Listener(boost::asio::io_context& context, net::ip::tcp::endpoint endpoint)
        : m_ioc{context}, m_acceptor{m_ioc, endpoint}, m_socket{m_ioc} {}
    void run() { loop(boost::beast::error_code{}); }
    void loop(boost::beast::error_code ec);

   private:
    net::io_context& m_ioc;
    net::ip::tcp::acceptor m_acceptor;
    net::ip::tcp::socket m_socket;
};
}  // namespace websocket
}  // namespace middleware
#endif  // MIDDLEWARE_LISTENER_HPP
