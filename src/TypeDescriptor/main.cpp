//
// Created by John Vishnefske on 7/30/22.
//
#include <boost/asio.hpp>
#include <boost/asio/ts/net.hpp>

#include "Listener.hpp"
#include "MessageRouter.h"
#include "MessageStreamDealer.h"

namespace net = boost::asio;

int main() {
    auto const threads = 4;

    // The io_context is required for all I/O
    net::io_context ioc;

    // Create and launch a listening port
    //    std::make_shared<middleware::websocket::Listener>(
    //        ioc, net::ip::tcp::endpoint{net::ip::make_address("0.0.0.0"),0} )
    //        ->run();
    middleware::websocket::Listener listener{
        ioc, net::ip::tcp::endpoint{net::ip::make_address("0.0.0.0"), 0}};
    // Capture SIGINT and SIGTERM to perform a clean shutdown
    net::signal_set signals(ioc, SIGINT, SIGTERM);
    signals.async_wait([&ioc](boost::system::error_code const&, int) {
        // Stop the io_context. This will cause run()
        // to return immediately, eventually destroying the
        // io_context and any remaining handlers in it.
        ioc.stop();
    });

    // Run the I/O service on the requested number of threads
    std::vector<std::thread> v;
    v.reserve(threads - 1);
    for (auto i = threads - 1; i > 0; --i)
        v.emplace_back([&ioc] { ioc.run(); });
    ioc.run();

    // (If we get here, it means we got a SIGINT or SIGTERM)

    // Block until all the threads exit
    for (auto& t : v) t.join();

    return 0;
}
