//
// Created by John Vishnefske on 7/30/22.
//
#include "MessageRouter.h"

#include <boost/asio/yield.hpp>
#include <boost/beast/core/error.hpp>

#include "Listener.hpp"
#include "MessageStreamDealer.h"  // spawned by Listener

namespace middleware {
namespace websocket {
void middleware::websocket::MessageRouter::onMessage(
    middleware::websocket::GenericMessage m,
    middleware::websocket::MessageWriter writable) {
    switch (m.op) {
        case Operations::publish:
            // todo we need some why to detect when streams go away, and
            // cleanup subscriber map.
            for (auto w : m_subscribers.at(m.topic)) {
                w(m);
            }
        case Operations::subscribe:
            m_subscribers.at(m.topic).push_back(writable);
        case Operations::announce:
            break;
    }
}
void middleware::websocket::Listener::loop(boost::beast::error_code ec) {
    BOOST_ASIO_CORO_REENTER(*this) {
        for (;;) {
            BOOST_ASIO_CORO_YIELD m_acceptor.async_accept(
                m_socket, std::bind(&Listener::loop, shared_from_this(),
                                    std::placeholders::_1));
            if (ec) {
                BOOST_THROW_EXCEPTION(std::runtime_error("accept"));
            } else {
                // Create the session and run it
                std::make_shared<MessageStreamDealer>(std::move(m_socket))
                    ->run();
            }

            // Make sure each session gets its own strand
            m_socket = boost::asio::ip::tcp::socket(net::make_strand(m_ioc));
        }
    }
}
}  // namespace websocket
}  // namespace middleware