/**
 * @file    main2
 * @author  John Vishnefske
 * @date    11/23/21
 * @brief   middleware
 *
 */

//
// Created by John Vishnefske on 11/23/21.

//
#include "main2.h"

#include <chrono>
#include <functional>
#include <future>
#include <iostream>
#include <memory>
#include <variant>
#include <vector>

class impl;
class B {
    class impl;

   private:
    std::shared_ptr<impl> p_a;

   public:
    B() {}
    ~B() {}

    //    B(A* a):p_a(a){}
    //    B()
    void print() { std::cout << "B" << std::endl; }
};
class impl {
   public:
    impl() { std::cout << "impl" << std::endl; }
    ~impl() { std::cout << "~impl" << std::endl; }
    void print() { std::cout << "::print" << std::endl; }
};
void old_test() {
    B b;
    b.print();
    std::cout << __FILE__ << " " << __FUNCTION__ << ":" << __LINE__
              << std::endl;
}

// requires C++17 or later
static_assert(std::variant_size_v<std::variant<int, double, std::string>> == 3,
              "variant_size_v is broken");

template <typename T>
class ChainableObserver {
   private:
    std::vector<std::function<void(T)>> m_observers;
    std::vector<std::future<void>> m_futures;

   public:
    template <class Then>
    ChainableObserver<Then> then(std::function<Then(T)> f
                                 //, std::function<void(void)> onError =
                                 //[](){std::cerr << "error" << std::endl;}
    ) {
        // todo does a return copy work?
        ChainableObserver<Then> c;
        m_observers.push_back([&c, f](T value) {
            try {
                c(f(value));
            } catch (...) {
                // onError();
                throw;
            }
        });
        return c;
    }
    void operator()(const T value) {
        std::cout << __FUNCTION__ << ":" << __LINE__
                  << " observers: " << m_observers.size() << std::endl;

        for (auto& observer : m_observers) {
            if (observer) {
                // observer(value);
                m_futures.push_back(std::async(observer, value));
            } else {
                std::cout << __FUNCTION__ << ":" << __LINE__
                          << " observer is not valid" << std::endl;
            }
        }
    }
    void awaitFor(std::chrono::duration<double> duration) {
        for (auto& future : m_futures) {
            // keep waiting until one of the futures times out, or all futures
            // are done.
            auto result = future.wait_for(duration);
            if (result == std::future_status::timeout) {
                return;
            }
        }
    }
};

void test_chainable_observer() {
#if 1
    ChainableObserver<double> c;
    std::function<double(double)> f = [](double x) {
        std::cout << "f(x) = " << x * x << std::endl;
        return x * x;
    };
    auto c2 = c.then(f);
    auto c3 = c2.then(f);
    std::cout << __FUNCTION__ << ":" << __LINE__ << std::endl;
    c(1.0);
    std::cout << __FUNCTION__ << ":" << __LINE__ << std::endl;
    c2(2.0);
    std::cout << __FUNCTION__ << ":" << __LINE__ << std::endl;
    c3(3.0);
    std::cout << __FUNCTION__ << ":" << __LINE__ << std::endl;
    c.awaitFor(std::chrono::microseconds(1));
    c2.awaitFor(std::chrono::duration<double>::zero());
    c3.awaitFor(std::chrono::duration<double>::zero());
#endif
}
int main() { test_chainable_observer(); }
