//
// Created by vis75817 on 7/21/2022.
//

#include "test_middleware2.h"

#include <boost/asio/co_spawn.hpp>
#include <boost/asio/coroutine.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/local/connect_pair.hpp>
#include <boost/asio/ts/net.hpp>
#include <boost/asio/ts/socket.hpp>
#include <boost/asio/ts/timer.hpp>
#include <boost/asio/use_future.hpp>
#include <boost/beast.hpp>
#include <boost/beast/_experimental/test/stream.hpp>
#include <boost/beast/_experimental/test/tcp.hpp>
#include <future>
#include <iostream>
#include <unordered_set>

#include "middleware.h"
#include "unit_test.hpp"
#undef BOOST_ASIO_HAS_CO_AWAIT
class reader : public std::enable_shared_from_this<reader>{
   public:
    void read(boost::beast::error_code ec, size_t read_length) {
        BOOST_CHECK(!ec);
        m_readBuffer.commit(read_length);
        int size_to_try = 8000;
        BOOST_ASIO_CORO_REENTER(m_coroutine) {
            BOOST_ASIO_CORO_YIELD m_stream.async_read_some(m_readBuffer.prepare(size_to_try), std::bind(&reader::read, shared_from_this(), std::placeholders::_1, std::placeholders::_2));
        }

    }
   private:
    boost::asio::coroutine m_coroutine;
    boost::beast::websocket::stream<boost::beast::tcp_stream> m_stream;
    boost::beast::flat_static_buffer<1000000> m_readBuffer;
}; /**
    * from
    * https://www.boost.org/doc/libs/1_71_0/doc/html/boost_asio/reference/async_compose.html
    */
struct async_echo_implementation
{
    boost::asio::ip::tcp::socket& socket_;
    boost::asio::mutable_buffer buffer_;
    enum { starting, reading, writing } state_;

    template <typename Self>
    void operator()(Self& self,
                    boost::system::error_code error = {},
                    size_t n = 0)
    {
        switch (state_)
        {
            case starting:
                state_ = reading;
                socket_.async_read_some(
                    buffer_, std::move(self));
                break;
            case reading:
                if (error)
                {
                    self.complete(error, 0);
                }
                else
                {
                    state_ = writing;
                    boost::asio::async_write(socket_, buffer_,
                                             boost::asio::transfer_exactly(n),
                                             std::move(self));
                }
                break;
            case writing:
                self.complete(error, n);
                break;
        }
    }
};
template <typename CompletionToken>
auto async_echo(boost::asio::ip::tcp::socket& socket,
                boost::asio::mutable_buffer buffer,
                CompletionToken&& token) ->
    typename boost::asio::async_result<
        typename std::decay<CompletionToken>::type,
        void(boost::system::error_code, size_t)>::return_type
{
    return boost::asio::async_compose<CompletionToken,
                                      void(boost::system::error_code, size_t)>(
        async_echo_implementation{socket, buffer,
                                  async_echo_implementation::starting},
        token, socket);
}