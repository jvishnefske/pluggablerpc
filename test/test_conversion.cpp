/**
 * @file    main
 * @author  vis75817
 * @date    11/19/21
 * @brief   pluggablerpc
 *
 */
/* includes -----------------------------------------------------------------*/

#include <simdjson.h>

#include <array>
#include <boost/beast/core.hpp>
//#include <boost/beast/websocket/stream_fwd.hpp>
#include "unit_test.hpp"
#include <iostream>
#include <string>
/* defines ------------------------------------------------------------------*/
/* typedefs -----------------------------------------------------------------*/
namespace net = boost::asio;
namespace websocket = boost::beast::websocket;
namespace beast = boost::beast;
/* globals ------------------------------------------------------------------*/
/* locals -------------------------------------------------------------------*/
/* function prototypes ------------------------------------------------------*/
/* functions ----------------------------------------------------------------*/

BOOST_AUTO_TEST_CASE(Test1) { BOOST_REQUIRE(1 == 1); }

BOOST_AUTO_TEST_CASE(arraySpecialization) {
    // attempt to determine array length
//    std::array a{1, 2, 3};  // since c++ 17
    std::array<int, 3> a{ {1, 2, 3} };
    BOOST_TEST(a.size() == 3u);
}


BOOST_AUTO_TEST_CASE(beastBufferOperations){
    enum {buffer_size=4096};
    beast::static_buffer<buffer_size> staticBuffer;
    beast::multi_buffer multiBuffer;
    beast::flat_buffer flatBuffer{};
    net::mutable_buffer mutableBuffer;
    net::const_buffer constBuffer;
    BOOST_TEST(flatBuffer.size() ==0u);
    BOOST_TEST(multiBuffer.size() ==0u);
    BOOST_TEST(staticBuffer.size() == 0u);
    BOOST_TEST(mutableBuffer.size() == 0u);
    BOOST_TEST(constBuffer.size() == 0u);

    std::string messageContent{"hello1"};
    std::istringstream iss{messageContent};
    // there should be a faster way to do this.
    mutableBuffer = net::buffer(messageContent, messageContent.size());
    BOOST_TEST(messageContent.size() == mutableBuffer.size());
    //beast::buffered_read_stream(iss, staticBuffer);
    std::string result;
    result = beast::buffers_to_string(mutableBuffer);
    BOOST_TEST(result == messageContent);
    result = std::string{};
    BOOST_TEST(result.empty());
}

// implemented in middleware.cpp.
namespace middleware {
simdjson::padded_string_view to_sv(boost::beast::flat_static_buffer_base& buf);
simdjson::ondemand::document_stream iterate_many(
    simdjson::ondemand::parser &p,
    boost::beast::flat_static_buffer_base &buffer
);
}
BOOST_AUTO_TEST_CASE(partialBufferReadAndWrite) {
    /**
     *  this is a brain storming experiment to see what data flow options between asio, and simdjson are feasible, and performant.
     *  1. use a dynamic_buffer to read as a simdjson::padded_string.
     *  2. check if the buffer contains a full, or partial message json object
     *  3. (optional) use simdjson::minify to copy partial message from the
     * dynamic buffer write to read similar to the functionality of dynamic_buffer::commit
     *  4. recover from attepted parsing of a partial message, and leave partial
     * message in the buffer.
     *  5. (optional) detect if the buffer contains a full object, then rollback, and parse with external parser if it is a publish operation.
     * 6. as an alternative to (3) we could first iterate to the end of an object, and then "process" part of the buffer.data out of the dynamic buffer into a simdjson::padded_string.
     * 7. (optional) as an alternative to (6) commit the data into the read area
     * of the dynamic buffer, but do not consume() until after the simdjson parse references have gone out of scope.
     *
     */
    enum max_size {
        max_size = simdjson::SIMDJSON_PADDING + 1024,
    };
    std::string partialJsonMessage = R"({"op": "announce" )";

    boost::beast::flat_static_buffer<max_size> source_buffer;
    /* 1. ================================================================= */
    const auto copy_count1 = boost::asio::buffer_copy(
        source_buffer.prepare(partialJsonMessage.size()),
        boost::asio::buffer(partialJsonMessage));
    source_buffer.commit(copy_count1);

    BOOST_TEST(copy_count1 == partialJsonMessage.size());
//    const simdjson::padded_string_view my_padded_sv = middleware::to_sv(source_buffer);
    simdjson::padded_string my_padded_sv(static_cast<char*>(source_buffer.data().data()),source_buffer.data().size());
    simdjson::ondemand::parser parser;
    {
//        std::cout << "partial read buffer1" << my_padded_sv.size()
//                  << my_padded_sv << std::endl;
        // The .iterate() throws a different error compared to iterate_many()
        //  INCOMPLETE_ARRAY_OR_OBJECT code vs throwing an exception when
        // de- referencing the begin() iterator.
        simdjson::ondemand::document_stream document_stream =
                                  parser.iterate_many(my_padded_sv);

        // check that the document_stream is empty
        // there is a use after heap free when callign
        auto iter = document_stream.begin();
        BOOST_REQUIRE(!(iter != document_stream.end()));
        // this conditional should never succeed but is included to show a
        // useful workflow.
        if(document_stream.begin() != document_stream.end()){
            simdjson::ondemand::document_reference document =
                * document_stream.begin();
            std::string_view operation;
            auto json_error = document.find_field("op").get(operation);
            if (json_error) {
                std::cout << std::to_string(int(json_error)) << "json_error";
            }
            BOOST_TEST(json_error == simdjson::error_code::SUCCESS);
        }
    }
    // now we add the rest of the message to the buffer this simulates a
    // read_some into the dynamic buffer. this also adds a partial second
    // message.
    /* 2. ================================================================= */
    {
        // for some reason creating a boost::asio::buffer from a c_str adds
        // a null terminator to the buffer which breaks simdjson.
        const std::string remaining_message_string{
            R"(, "type":"brightness", "topic":"partial message"}{)"};
        const auto remaining_message_buffer =
            boost::asio::buffer(remaining_message_string);
        std::string_view operation;
        std::string_view type;
        std::string_view topic;
        // a second copy should be required to copy the second message.
        source_buffer.commit(boost::asio::buffer_copy(
            source_buffer.prepare(remaining_message_buffer.size()),
            remaining_message_buffer));
        /**
         * since we have a partial second message, use the document iterator,
         * and dereference the iterator to get the first document.
         */
        // this is the simdjson::padded_string_view that is used to read the
        // buffer.
//         simdjson::padded_string_view s =
//            middleware::to_sv(source_buffer);
        simdjson::ondemand::document_stream documents = middleware::iterate_many(parser, source_buffer);
            //parser.iterate_many(s);
        simdjson::ondemand::document_reference document2 =
            *documents.begin();  // get the first document
        const char* p_parser_start_position = document2.current_location();
        BOOST_TEST(source_buffer.size() ==
              remaining_message_string.size() + partialJsonMessage.size());
        BOOST_TEST(document2.find_field("op").get(operation) ==
              simdjson::error_code::SUCCESS);
        BOOST_TEST(operation == "announce");

        BOOST_TEST(document2.find_field("type").get(type) ==
              simdjson::error_code::SUCCESS);
        BOOST_TEST(type == "brightness");
        BOOST_TEST(document2.find_field("topic").get(topic) ==
              simdjson::error_code::SUCCESS);
        BOOST_TEST(topic == "partial message");
        // get the number of bytes in the
        const char* p_parse_location = document2.current_location();
        // the plus one here is important since current location points to the
        // last character in the previous json object, and we want to include that
        // character in the consume().
        const auto byte_count = static_cast<std::size_t>(p_parse_location - p_parser_start_position) +1u;
        // try to compute byte_count from the buffer, and document only. this is useful for get_some(socket,document,buffer)
//        const auto byte_count2 = documents.begin().source().size();
//        BOOST_TEST(byte_count2 == byte_count);
        // we should have one byte left in the stream after this object is completely parsed.
        // the question is how do we know that the document pointer is at the end of the current json object?
        BOOST_TEST(( source_buffer.size() - byte_count) == 1u);
        source_buffer.consume(byte_count);
        // check the first character of the buffer.
        BOOST_REQUIRE(source_buffer.data().size() > 0); // we don't want to dereference a zero length container.
        BOOST_TEST(static_cast<char*>(source_buffer.data().data())[0] == '{');
        BOOST_TEST(source_buffer.size() == 1u);

        const std::string source = "hello";
        const std::string_view result{source};
        auto size = (result.data()+1L) - source.data();
        BOOST_TEST(size == 1);


        // parsing an empty buffer may be useful for constructor.
        boost::beast::flat_static_buffer<1000> buf{};
        simdjson::ondemand::document_stream docs{parser.iterate_many(middleware::to_sv(buf))};
        BOOST_CHECK(!(docs.begin() != docs.end()));
    }
}
#if 0
//some work was done to implement this in quic, but untested.
// create an instance of quic server
BOOST_AUTO_TEST_CASE(Test 2", "[main]) {
    middleware::MsQuicServer server;
    server.setConnectionHandler([] (middleware::BinaryData d) {
        std::cout << "data received" <<std::endl;
    });
    //server.listen(5);
    std::cout << "server started" << std::endl;
    middleware::MsQuicClient client;
    std::cout << "waiting for client" << std::endl;
    auto result = std::async(std::launch::async , [&] {
        std::cout << "client connecting" << std::endl;
        return 42;
    });
    std::cout << "waiting for result" << std::endl;
    //auto res = result.get();

    std::cout << "server stopped" << std::endl;
}

BOOST_AUTO_TEST_CASE(Test 3", "[main]) {
    middleware::WebsocketServer server;
    server.setConnectionHandler([] (middleware::BinaryData d) {
        std::cout << "data received" <<std::endl;
    });
    server.listen(5);
    std::cout << "server started" << std::endl;
    middleware::WebsocketClient client;
    std::cout << "waiting for client" << std::endl;
    auto result = std::async(std::launch::async , [&] {
        std::cout << "client connecting" << std::endl;
        return 42;
    });
    std::cout << "waiting for result" << std::endl;
    //auto res = result.get();

    std::cout << "server stopped" << std::endl;
}
#endif
