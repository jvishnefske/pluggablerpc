#include "unit_test.hpp"
#include "middleware.h"
#define SIMDJSON_DEVELOPMENT_CHECKS 1 // additional checks
#include <boost/asio/ts/net.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/local/connect_pair.hpp>
#include <boost/asio/co_spawn.hpp>
#include <boost/asio/use_future.hpp>
#include <boost/beast.hpp>
#include <iostream>
#include <unordered_set>
#include <boost/beast/_experimental/test/stream.hpp>
#include <boost/beast/_experimental/test/tcp.hpp>
#include <boost/variant2.hpp>
using namespace boost::variant2;
// Includin this is possibly not an ideal situation, but this is
// a hack to keep the expensive imports out of the public header.
#include "../src/middleware.cpp"
#include "test_middleware2.h"
/**
 * overview of publish story.
 *
 * Publisher<DataType>("topic").publish(obj);
 * SerDes<JsonType, VariantTypes...>({"type labels"...}).dump(obj)-> buffer;
 * async_write(buffer, token)
 * .....websocket
 * async_read(buffer, token)
 * on_read()
 * dispatchMessage(buffer)
 * if ( operation == "publish" ) std::string_view topic = topicFromMsg();
 *   -- determine which streams/ Subscriber objects should receive message based
 * on topic label, and local map
 *   -- all relevant streams receive a buffer.
 *   -- std::string typelabel = topicToTypeMap.at(topic);
 *   -- msgVariant = SerDes<...>("types"...).loads(typeLabel, MessageDocument);
 *   -- Subscribers<Type>("topic").dispatch(msgVariant) is sent by dispatcher
 *   -- auto o = std::get<SubscriptionType>(msgVariant)
 *   -- onNotifySubscriber<Type>(o);
 *
 * SerDes<...>.loads(
 */

/**
 * there are several options for how to structure this.
 *  the main motivation would be for late subscribers to get the current stream,
 *  also late publishers should establish notification to all clients currently running.
 *  -- one server per publisher topic, and announce via service discovery.
 *  -- one ws server per subscriber, and publishers discover clients.
 *  -- radio antenna, or req rep dealer router ZMQ options
 *  I tend to like option two as it forces ws clients to continuously update
 *      network state, and client discovery/ falloff.
 */

/**
 *  in order for websocket discovery to work there must be an existing
 *  service discovery through central orchestration, or avahi
 */



namespace net = boost::asio;
namespace websocket = boost::beast::websocket;
namespace beast = boost::beast;
enum{buffer_size = 2000000};
//
// Created by vis75817 on 6/9/2022.
//
// the example strings include simdjson padding.

struct ThingToConvert{int a,b;};
template<class StringType>
void toString(ThingToConvert obj, StringType &s);
template<class StringType>
void fromString(StringType s, ThingToConvert &obj);


template<class... variantTypes>
class TypeTracker{
   public:
     constexpr TypeTracker(std::array<std::string_view, sizeof...(variantTypes)> typeNames, std::variant<variantTypes...>):
         m_typeNames{typeNames}
     {
     }
    constexpr explicit TypeTracker(std::array<std::string_view,sizeof...(variantTypes)> typeNames): m_typeNames{typeNames}{

    }
    constexpr TypeTracker() = default;
    constexpr size_t indexOfIndex(int i){
        return m_typeArray.at(i).index();
    }
#if 0
using MyVariant = std::variant<variantTypes...>;
    constexpr size_t indexOfLabel(const std::string_view typeLabel){
        const auto result = std::find(m_typeNames.begin(), m_typeNames.end(), typeLabel);
        if(result==m_typeNames.end()){
            throw std::runtime_error("invalid type label lookup");
        }
        // convert iterator to integer.
        const auto index = result - m_typeNames.begin();
        return index;
    }
    constexpr bool containsLabel(const std::string_view typeLabel){
        const auto iterator = std::find(m_typeNames.begin(), m_typeNames.end(), typeLabel) != m_typeNames.end();
        const bool is_found = (iterator != m_typeNames.end());
        return is_found;
    }
    constexpr MyVariant loads(const std::string_view typeLabel, const std::string_view message){
        // returns parsed document using a json which has a similar interface
        // to modern json.
        const auto index = indexOfLabel(typeLabel);
        const auto typeExample = m_typeArray.at(index);
        return std::visit([message](auto t){
            auto document = Json::parse(message);
            return MyVariant {document.get<decltype(t)>()};
        }, typeExample);
    }
    template<class Buffer>
    constexpr void dumps(const MyVariant v, Buffer & destination);
#endif
   private:
    template<class T>
    std::variant<variantTypes...> generatorImpl(T){
        return T{};
    }
    std::variant<variantTypes...> generateAtIndex(int i){
        // this is much less general than std::variant::emplace<I>
        // but may be useful for collecting json conversion functions.
        return generatorImpl(m_typeArray.at(i));
    }

    //const std::variant<variantTypes...> m_container;
    std::array<std::string_view,sizeof...(variantTypes)> m_typeNames;
    static constexpr std::array<std::variant<variantTypes...>,sizeof...(variantTypes)> m_typeArray{variantTypes{}...};
    // now create an array of functions which return the specified type.
    static constexpr std::array<int, sizeof...(variantTypes)> m_generators{};
    //static constexpr std::array m_generatorArray{[]{return std::variant<variantTypes...>{ variantTypes{}}; }...} ;
};

template  <class BufferType, class JsonType>
struct GenericMessage{
    const BufferType rawMessage;
    std::string_view operation;
    // consider interface of simdjson ondemand document_reference
    JsonType documentReference;
};

template<class TargetType>
class Schema{
    template<class AttributeType>
    void mapTo(std::string /*attributeName*/, AttributeType /*member*/){
        // provides ability for bijunctive mapping between serialized objects, and c++ class members.
        // the intent is to construct schema information which can be utilized for both serialization, and deserialziation.
        // ideally this could be composable, and allow simple data types to be composed into composite types.
        throw std::runtime_error("not implemented");
    }
};
BOOST_AUTO_TEST_CASE(test_type_tracker)
{
    TypeTracker<int,float,std::string_view> t{{"","",""}};
    BOOST_TEST(t.indexOfIndex(1)==1u);
}

#if 0
BENCHMARK_ADVANCED("generate new message strings")(Catch::Benchmark::Chronometer meter){
    std::vector<Catch::Benchmark::storage_for<simdjson:padded_string> padded_string;
    meter.measure[&]{
        std::cout <<"add json message dispatch here." <<std::endl;
    };
}
BENCHMARK_ADVANCED("benchmark on part in two steps")(Catch::Benchmark::Chronometer meter{
    // setup
    std::vector<Catch::Benchmakr::storage_for<simdjson::>> storage(meter.runs());
    for( auto&&: o:storage){
        o.cons
    }
    // execution
    meter.measure[&](int i){
        //  thing i to measure
    }

}
#endif




struct ProffOfConceptMessageRouter{
    struct TcpStream{
        // AsyncReader, AsyncWriter
        // async_reader of message variant
    };
};

#if ENABLE_SLOW_BENCHMARK
// parse and route messages between connections.
BOOST_AUTO_TEST_CASE(command router happy path){
    // connect 2 to router.
    // publish message. ensure no message shows up where not subscribed.
    BENCHMARK("announce message"){

    };
    BENCHMARK("dummy message"){

    };
}
#endif
using namespace std::literals::string_view_literals;
BOOST_AUTO_TEST_CASE(indexOfIndex){
    //verify we are able to make a variant type from a runtime integer.
   TypeTracker<int,bool,double> tracker{{"int","bool","double"}};
   static_assert(tracker.indexOfIndex(1) == 1);
   // for each value attempt to retrieve a variant of that type.
   for(auto i=0u; i<3; i++){
       BOOST_TEST(i == tracker.indexOfIndex(i));
   }
}
BOOST_AUTO_TEST_CASE(websocketLoopback){
    // create a socket server

    net::io_context context;
    beast::tcp_stream socket{context};
    //server
    websocket::stream<net::ip::tcp::socket> server{context};
    enum{buffer_size = 2000000};
    //boost::beast::multi_buffer <buffer_size> accept_buffer;
    std::cout <<"server waiting to accept"<<std::endl;
    server.async_accept( [](boost::beast::error_code ec){
        BOOST_REQUIRE(!ec); // fail on error.
        boost::beast::static_buffer<buffer_size> inputBuffer;
//        socket.async_read_some(inputBuffer, [](boost::beast::error_code ec, std::size_t bytes_transfered){
//            REQUIRE_FALSE(ec);
//            std::cout << "server websocket read returned byte count: " << bytes_transfered << "." << std::endl;
//        });
        std::cout <<"accepted connection" << std::endl;
    });
//    websocket::stream<net::ip::tcp::socket> server{context};
//    setsocket<beast::tcp_stream> server{};
//    server.async_accept([&](boost::beast::error_code ec){
//        REQUIRE_FALSE(ec); // fail on error.
//        boost::beast::flat_static_buffer<buffer_size> inputBuffer;
//        //socket.async_read_some(inputBuffer, [](boost::beast::error_code ec, std::size_t bytes_transfered){
//        //            REQUIRE_FALSE(ec);
//        //            std::cout << "server websocket read returned byte count: " << bytes_transfered << "." << std::endl;
//        //});
//        //server.next_layer().async_receive(inputBuffer, [](beast::error_code ec, std::size_t read_length){
//        //    REQUIRE_FALSE(ec);
//        //    BOOST_TEST(read_length == multiMessage.size());
//        //});
//        // get the socket we accepted, and do something with it.
//        std::cout <<"accepted connection" << std::endl;
//    });

    //client with strand for sequencing concurrent method calls.
    websocket::stream<beast::tcp_stream> client{net::make_strand(context)};
//    client.next_layer().handshake(net::ssl::stream_base::client);
    //net::ip::tcp::resolver resolver{context};

//    auto const resource = resolver.resolve("localhost",0);
//    REQUIRE_NOTHROW( net::connect(client.next_layer(), resource) );
//    boost::beast::flat_static_buffer<buffer_size> client_read_buffer;
//    REQUIRE_NOTHROW( client.write(net::buffer(std::string("hello1"))) );
//    REQUIRE_NOTHROW( client.read(client_read_buffer) );
//    REQUIRE_NOTHROW( client.next_layer().close(websocket::close_code::normal) );
    std::cout << "websocket client closed." << std::endl;
}
namespace net = boost::asio;
namespace websocket = boost::beast::websocket;
namespace beast = boost::beast;
namespace{
BOOST_AUTO_TEST_CASE(test_websocket, * boost::unit_test::disabled()) {
    typedef boost::asio::detail::socket_option::integer<SOL_SOCKET, SO_RCVTIMEO> rcv_timeout_option;
    net::io_context context;
    websocket::stream<beast::tcp_stream> ws{net::make_strand(context)};
    const auto endpoint = net::ip::tcp::endpoint(net::ip::tcp::v4(), 0);
    net::ip::tcp::acceptor acceptor(context,endpoint);
    const auto port = acceptor.local_endpoint().port();
    std::cout << "port: " << port << std::endl;
    acceptor.set_option(rcv_timeout_option{ 1 });
    acceptor.async_accept(context, [](boost::beast::error_code error, net::ip::tcp::socket socket ){
        BOOST_CHECK(!error);
        websocket::stream<beast::tcp_stream> server(std::move(socket));
        std::cout << "server: "  << std::endl;
        server.accept();
        std::cout << "server accepted websocket. " << std::endl;
    });
    net::ip::tcp::resolver resolver(context);
    BOOST_CHECK_NO_THROW(beast::get_lowest_layer(ws).connect(
        resolver.resolve("127.0.0.1", std::to_string(port))));
    std::string hostname{"localhost"};
    std::string request_target{"/"};
    std::cout << "attempting handshake" << std::endl;
    ws.async_handshake(hostname, request_target,
                       [](boost::system::error_code ec) {
                           if (ec.failed()) {
                               std::cout << "failed async handshake "
                                         << ec.message() << std::endl;
                               return;
                           }
                           std::cout << "async handshake complete" << std::endl;
                       });
    context.run(); // seems to run forever.
    std::cout << "test complete" << std::endl;
}
}// namespace


#if 0
BOOST_AUTO_TEST_CASE(synchronousWebsocketLoopback){
    //context
    net::io_context context;
    net::ip::tcp::resolver resolver(context);

    //websocket https://www.boost.org/doc/libs/1_78_0/libs/beast/doc/html/beast/using_websocket.html
    // tcp_stream is constructed with a new strand.

    net::io_context server_context;

    //server
    net::ip::tcp::acceptor acceptor(server_context);
    const auto server_endpoint = net::ip::tcp::endpoint(net::ip::tcp::v4(), 0);
    acceptor.open(server_endpoint.protocol());
    acceptor.bind(server_endpoint);
    acceptor.listen();
    const auto random_port = acceptor.local_endpoint().port();
    std::cout << "listening on port "<<  random_port <<std::endl;

    auto server_future = std::async([](auto acceptor){
        websocket::stream<beast::tcp_stream> server(acceptor.accept());
        std::cout << "server connection accepted" <<std::endl;
        //set timeout for handshake, and idle.
        // beast::websocket::stream_base::timeout opt{std::chrono::seconds(1)/* handshake_timeout*/,websocket::stream_base::none()/*idle timeout*/, false};
        server.accept();
        std::cout << "server websocket accepted" <<std::endl;
        // consider checking for timeout if(ec == beast::error::timeout)
        // receive message
        beast::flat_buffer read_buffer;
        server.read(read_buffer);
        BOOST_TEST(read_buffer.size() >0);
        return beast::buffers_to_string(read_buffer.data());
    }, std::move(acceptor) );
    //client
    websocket::stream<beast::tcp_stream> client(net::make_strand(context));
    boost::beast::websocket::response_type res;
    std::cout << "resolving address" <<std::endl;
    const auto client_endpoint = resolver.resolve("127.0.0.1", std::to_string(random_port));
    std::cout << "client connecting" << std::endl;
    client.next_layer().connect(client_endpoint);
    std::cout << "connected" <<std::endl;
    client.handshake("127.0.0.1", "/");
    std::cout << "completed client handshake" <<std::endl;
    // could add additional checks on the server response.
    BOOST_CHECK_NO_THROW(res.result());
    std::string message_contents{"hello world!!"};
    client.write(net::buffer(message_contents));
    const auto receiveTimeout = std::chrono::milliseconds(10);
    const auto result = server_future.wait_for(receiveTimeout);
    BOOST_REQUIRE(result == std::future_status::ready);
    BOOST_REQUIRE(server_future.valid());
    std::string response = server_future.get();
    BOOST_TEST(response == message_contents);
    // make a MessageStream from the client.
    //client.next_layer().close();
};
BOOST_AUTO_TEST_CASE(synchronousMessageStreamLoopback){

    //context
    net::io_context context;
    net::ip::tcp::resolver resolver(context);

    //websocket https://www.boost.org/doc/libs/1_78_0/libs/beast/doc/html/beast/using_websocket.html
    // tcp_stream is constructed with a new strand.

    net::io_context server_context;

    //server
    net::ip::tcp::acceptor acceptor(server_context);
    const auto server_endpoint = net::ip::tcp::endpoint(net::ip::tcp::v4(), 0);
    acceptor.open(server_endpoint.protocol());
    acceptor.bind(server_endpoint);
    acceptor.listen();
    const auto random_port = acceptor.local_endpoint().port();
    std::cout << "listening on port "<<  random_port <<std::endl;
    auto server_future = std::async([](auto acceptor){
        websocket::stream<beast::tcp_stream> server(acceptor.accept());
        std::cout << "server connection accepted" <<std::endl;
        //set timeout for handshake, and idle.
        // beast::websocket::stream_base::timeout opt{std::chrono::seconds(1)/* handshake_timeout*/,websocket::stream_base::none()/*idle timeout*/, false};
        server.accept();
        std::cout << "server websocket accepted" <<std::endl;
        // consider checking for timeout if(ec == beast::error::timeout)
        // receive message
        middleware::MessageStream serverStream(std::move(server));
        std::future<middleware::PartiallyParsedMessage> message_future = serverStream.async_read_message(net::use_future);
            //[](beast::error_code, middleware::PartiallyParsedMessage){std::cout << "received message at server." << std::endl;});

        auto timeout = message_future.wait_for(std::chrono::seconds(1));
        BOOST_REQUIRE(timeout == std::future_status::ready);
        BOOST_REQUIRE(message_future.valid());
        return message_future.get();
    }, std::move(acceptor) );
    //client

    websocket::stream<beast::tcp_stream> client(net::make_strand(context));
    boost::beast::websocket::response_type res;
    std::cout << "resolving address" <<std::endl;
    const auto client_endpoint = resolver.resolve("127.0.0.1", std::to_string(random_port));
    std::cout << "client connecting" << std::endl;
    client.next_layer().connect(client_endpoint);
    std::cout << "connected" <<std::endl;
    client.handshake("127.0.0.1", "/");
    std::cout << "completed client handshake" <<std::endl;
    auto clientMessageStream = middleware::MessageStream(std::move(client));
    clientMessageStream.async_write_some(R"({"op": "announce"})", net::use_future);
    const auto receiveTimeout = std::chrono::milliseconds(10);
    const auto result = server_future.wait_for(receiveTimeout);
    BOOST_REQUIRE(result == std::future_status::ready);
    BOOST_REQUIRE(server_future.valid());
    middleware::PartiallyParsedMessage response = server_future.get();
    BOOST_TEST(response.messageFields.index() == 2); // announce message
    BOOST_TEST(response.messageView == "announce");
    // make a MessageStream from the client.

    //client.next_layer().close();

};
#endif
BOOST_AUTO_TEST_CASE(boostTestSocketConnection){
    std::string message_part1 = R"({"op": "announce" )";
    const std::string message_part2{
        R"(, "type":"brightness", "topic":"partial message"}{)"};
    boost::asio::io_context context;
    namespace test = boost::beast::test;
    test::stream input{context};
//
//    // now let us make a json reader, and see what happens.
    middleware::StreamJsonReaderImpl reader{};
//    CHECK_NOTHROW(input.append(message_part1));
//    BOOST_TEST(input.str() == message_part1);
//    CHECK_NOTHROW(input.append(message_part2));
//    simdjson::ondemand::document doc;
//    CHECK_NOTHROW(reader.read_some(input, doc));
//    BOOST_TEST(doc.find_field("op").get_string().value() == "announce" );
};

#if BOOST_ASIO_HAS_CO_AWAIT && 0
BOOST_AUTO_TEST_CASE(MessageSocketJsonParsing){

//    stream2.append(R"({"op":"announce","topic": "my-topic", "type": "brightness"})");


//context
net::io_context context;
net::ip::tcp::resolver resolver(context);

//websocket https://www.boost.org/doc/libs/1_78_0/libs/beast/doc/html/beast/using_websocket.html
// tcp_stream is constructed with a new strand.

net::io_context server_context;

//server
net::ip::tcp::acceptor acceptor(server_context);
const auto server_endpoint = net::ip::tcp::endpoint(net::ip::tcp::v4(), 0);
acceptor.open(server_endpoint.protocol());
acceptor.bind(server_endpoint);
acceptor.listen();
const auto random_port = acceptor.local_endpoint().port();
std::cout << "listening on port "<<  random_port <<std::endl;
//std::future<boost::asio::ip::tcp::socket> server_future = acceptor.async_accept(net::use_future);
const auto co_server = [&acceptor, &server_context]()-> net::awaitable<void> {
    websocket::stream<beast::tcp_stream> server{
    net::co_await acceptor.async_accept(net::use_awaitable) };
    // accept websocket handshake.
    std::cout << "tcp socket handshake accepted" << std::endl;
    net::co_await server.async_accept(net::use_awaitable);
    std::cout << "server websocket connection established." << std::endl;
};
auto future_server = net::co_spawn(context, co_server(), net::use_future);
std::thread t{[&context]{context.run();}};

//    std::async([](auto acceptor){
//    websocket::stream<beast::tcp_stream> server(acceptor.accept());
//    std::cout << "server connection accepted" <<std::endl;
//    //set timeout for handshake, and idle.
//    // beast::websocket::stream_base::timeout opt{std::chrono::seconds(1)/* handshake_timeout*/,websocket::stream_base::none()/*idle timeout*/, false};
//    server.accept();
//    std::cout << "server websocket accepted" <<std::endl;
//    // consider checking for timeout if(ec == beast::error::timeout)
//    // receive message
//    beast::flat_buffer read_buffer;
//    server.read(read_buffer);
//    BOOST_TEST(read_buffer.size() >0);
//    return beast::buffers_to_string(read_buffer.data());
//}, std::move(acceptor) );
//client
websocket::stream<beast::tcp_stream> client(net::make_strand(context));
boost::beast::websocket::response_type res;
std::cout << "resolving address" <<std::endl;
const auto client_endpoint = resolver.resolve("127.0.0.1", std::to_string(random_port));
std::cout << "client connecting" << std::endl;
client.next_layer().connect(client_endpoint);
std::cout << "connected" <<std::endl;
//client.handshake("127.0.0.1", "/");
//std::cout << "completed client handshake" <<std::endl;
// could add additional checks on the server response.
CHECK_NOTHROW(res.result());
std::string message_contents{"hello world!!"};
//client.write(net::buffer(message_contents));
const auto receiveTimeout = std::chrono::milliseconds(10);
//const auto result = server_future.wait_for(receiveTimeout);
//BOOST_REQUIRE(result == std::future_status::ready);
//BOOST_REQUIRE(server_future.valid());
//std::string response = server_future.get();
//BOOST_TEST(response == message_contents);
client.next_layer().close();
context.stop();
t.join();
}
#endif

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__) || 1
// skipping unix socket connection on Windows
#elif defined(BOOST_ASIO_HAS_LOCAL_SOCKETS)
BOOST_AUTO_TEST_CASE(json streamer class","[this only works on posix cause unix socket support]){
    // create a pair of local streams for injection purposes.
    // may be useful to allow StreamJsonReader to connect to multiple stream types
    using boost::asio::local::stream_protocol;
    boost::asio::io_context context;
    stream_protocol::socket dummyStream{context};
    stream_protocol::socket documentStream{context};
    boost::beast::error_code error;

    CHECK_NOTHROW(boost::asio::local::connect_pair(dummyStream, documentStream, error));
    CHECK_FALSE(error);
    boost::beast::multi_buffer readBuffer;
//    dummyStream.async_read_some(readBuffer, []( boost::beast::error_code ec,size_t size){
//        CHECK_FALSE(ec.failed());
//        std::cout << "read " << size << "bytes. "<< std::endl;
//    });
    CHECK_NOTHROW(
    documentStream.write_some(boost::asio::buffer("hello"))
    );
}
BOOST_AUTO_TEST_CASE(middleware accept){
using  boost::asio::ip::tcp;
boost::asio::io_context io_context;
tcp::acceptor acceptor(io_context, {tcp::v4(), 55554});
BOOST_TEST(acceptor.is_open());
//tcp::socket socket = acceptor.accept();
//io_context.run();
//BOOST_TEST(socket.is_open());
}

#else // defined(BOOST_ASIO_HAS_LOCAL_SOCKETS)
# error Local sockets not available on this platform.
#endif // defined(BOOST_ASIO_HAS_LOCAL_SOCKETS)
using tcp = boost::asio::ip::tcp;
