#include <simdjson.h>
#include "middleware.h"
#include "unit_test.hpp"
#include <iostream>
#include <boost/variant2.hpp>
using namespace boost::variant2;

const auto multiMessage =
    R"({"op":"publish","topic":"voltage","msg":{"data":1,1}}
{"op":"announce", "topic":"measurement", "type":"announce_type"}{"op":"subscribe","topic":"color", "type":"mytype"})"_padded;
BOOST_AUTO_TEST_CASE(middlewareCommandParser) {
    simdjson::padded_string str1{};
    simdjson::ondemand::parser p{};
    simdjson::ondemand::document_stream docs = p.iterate_many(str1);
    BOOST_REQUIRE(!(docs.begin() != docs.end()));
}

BOOST_AUTO_TEST_CASE(jsonImplicitConversion) {
    // load from `twitter.json` file:
    simdjson::dom::parser parser;
    std::string source = "{\"hello\": 1,\"search_metadata\":{\"count\":2}}";
    simdjson::dom::element tweets = parser.parse(source);
    std::cout << tweets["search_metadata"]["count"] << " results." << std::endl;

    // Parse and iterate through an array of objects
    auto abstract_json = R"( [
        {  "12345" : {"a":12.34, "b":56.78, "c": 9998877}   },
        {  "12545" : {"a":11.44, "b":12.78, "c": 11111111}  }
        ] )"_padded;

    for (simdjson::dom::object obj : parser.parse(abstract_json)) {
        for (const auto key_value : obj) {
            std::cout << "key: " << key_value.key << " : ";
            simdjson::dom::object innerobj = key_value.value;
            std::cout << "a: " << double(innerobj["a"]) << ", ";
            std::cout << "b: " << double(innerobj["b"]) << ", ";
            std::cout << "c: " << int64_t(innerobj["c"]) << std::endl;
        }
    }
}
BOOST_AUTO_TEST_CASE(jsonParseExamples) {
    // use simdjson to parse several message operations.
    simdjson::ondemand::parser parser;
    simdjson::dom::parser domparser;
    simdjson::dom::document_stream dom_stream;
    //    domparser.parse_many
    simdjson::ondemand::document_stream results =
        parser.iterate_many(multiMessage);
    int count = 0;

    for (auto document : results) {
        count++;
        // this parser should need to reflect the ordering since simd iterates
        // through the string, and does not create an intermediate tree object.
        const std::string_view operation = document["op"];
        // for each operation check the values.
        if (bool(operation == "publish")) {
            BOOST_TEST(document["topic"].get_string().value() == "voltage");
            BOOST_TEST(simdjson::to_json_string(document["msg"]).value() ==
                  "{\"data\":1,1}");
        } else if (operation == "subscribe") {
            BOOST_TEST(document["topic"].get_string().value() == "color");
            BOOST_TEST(document["type"].get_string().value() == "mytype");
        } else if (operation == "announce") {
            BOOST_TEST(document["topic"].get_string().value() == "measurement");
            BOOST_TEST(document["type"].get_string().value() == "announce_type");
        } else {
            BOOST_REQUIRE(false);  // unexpected "op".
        }
        // not try to get an invalid key, and check that a failure is issued as
        // expected.
        BOOST_TEST(document["invalid_key"]
                  .error());  // == simdjson::error_code::NO_SUCH_FIELD);
    }
    BOOST_REQUIRE(count == 3);

    /**
     * for the actual implementation it may be necessary to either use have
     * websocket write to a padded_string, or somehow let simdjson read from a
     * beast static_buffer, and check if the padding is adequate. another
     * potential would be using simdjson::minify to copy from a static_buffer
     * into a padded_string, and then parsing the padded_string using either the
     * dom, or ondemand frontend. note that simdjson::SIMDJSON_PADDING may be
     *
     */
}

BOOST_AUTO_TEST_CASE(ondemandJsonStreaming) {
    namespace ondemand = simdjson::ondemand;
    // example from documentation
    auto json = R"({ "foo": 1 } { "foo": 2 } { "foo": 3 } )"_padded;
    ondemand::parser parser;
    ondemand::document_stream docs = parser.iterate_many(json);
    for (auto doc : docs) {
        std::cout << doc["foo"] << std::endl;
    }
}
struct MessageToSend1 {
    std::string a;
    int b;
    float c;
};
//template <class JsonType, class MessageType>
//std::string dumps(MessageType msg) {}

BOOST_AUTO_TEST_CASE(simdMessageSerailization) {}
//using namespace std::literals::string_view_literals;
// slow test disabled by default [.]
BOOST_AUTO_TEST_CASE(simdMessageParsingPerformance) {
    const auto msg =
//        R"({"op": "advertise", "id": "1234567890", "topic": "2", "type": "brightness"}{}{}{})"_padded;
        R"({"op":"advertise", "id":"1234567890","topic": "second star from the right and straight on until sunrise/luminosity","type":"brightness"})"_padded;
    //    msg.reserve(simdjson::SIMDJSON_PADDING+msg.size());
    simdjson::ondemand::parser parser;
    std::string min_output;
    min_output.reserve(simdjson::SIMDJSON_PADDING + msg.size());
#if 0 // disable benchmark for now to redude dependencies

    BENCHMARK("simdjson minify") {
        size_t new_size{};
        const auto error_code = simdjson::minify(msg.data(), msg.size(),
                                                 min_output.data(), new_size);
        if (error_code != simdjson::error_code::SUCCESS) {
            throw std::runtime_error("parse error while minifying");
        }
    };
    std::cout << " msg: " << msg << std::endl;
#define SIMDJSON_GET_REF
    BENCHMARK("parse static message") {
        // no idea why the iterate many version throws an exception, but seems
        // to parse out the correct values if the exception is caught, and ignored.
//        simdjson::ondemand::document_reference doc = *(  static_cast<simdjson::ondemand::document_stream>( parser.iterate_many(msg)).begin());
        simdjson::ondemand::document doc_byvalue = static_cast<simdjson::ondemand::document>( parser.iterate(msg));
        simdjson::ondemand::document_reference doc(doc_byvalue);
#ifdef SIMDJSON_GET_REF
        std::string_view op;
//        BOOST_TEST(doc.find_field("op").get(op) == simdjson::error_code::SUCCESS);
        if(doc.find_field("op").get(op) ){throw std::runtime_error( "op returned parse error.");}

#else
        const std::string_view op = doc.find_field("op").get_string();
#endif
        if (op != "advertise"sv) {
            throw std::runtime_error(
                "unexpected op while parsing with simdjson");
        }
#ifdef SIMDJSON_GET_REF
        std::string_view topic;
        std::string_view type;
        if(doc.find_field("topic").get(topic)  ){throw std::runtime_error(
                "unexpected error while parsing with simdjson");};
#else
        const std::string_view topic =  doc.find_field("topic").get_string(); bool e1 = (topic.size()==0);
#endif
#ifdef SIMDJSON_GET_REF
        if(doc.find_field("type").get(type)){throw std::runtime_error("");}
#else
        const std::string_view type = doc.find_field("type").get_string();

        bool e3=false;
#endif

    };
#endif // benchmark
}

/**
 * implements a pubsub message parser interface to extract the operation
 * tag, and a limited number of required tags based on a few operation types.
 */
struct JsonMesssageIterator {
    using Message = variant<middleware::AnnounceMessage, middleware::SubscribeMessage>;
    void async_get_message();
    simdjson::ondemand::document m_document;
    std::string_view operation;
    // this constructor does not benifit from the efficiency of
    // reusing the parser.
    JsonMesssageIterator(simdjson::padded_string_view buffer){
        JsonMesssageIterator(buffer, simdjson::ondemand::parser{});
    }
    JsonMesssageIterator(simdjson::padded_string_view buffer, simdjson::ondemand::parser p)
        :
          m_document{p.iterate(buffer)},
          operation{m_document.find_field("op").get_string()}
    {}
    std::string_view getType(){
        return m_document.find_field("type").get_string();
    }
    std::string_view getTopic(){
        return m_document.find_field("topic").get_string();
    }
};