#include <array>
#include <future>
#include <variant>
#include <iostream>
#include <nlohmann/json.hpp>
#include "middleware.h"
#include "unit_test.hpp"

// implementation to be moved to a new module prior to deployment
namespace {

template<class T>
class TypeDescriptor;

template<auto... MemberPointers>
struct MemberList{
    template<typename T, typename F>
    constexpr static auto apply(F f, T v){
        return f(v.*MemberPointers...);
    }
};

//template<typename T>
//concept Described = requires{TypeDescriptor<T>{};};

template <class T>
class jsonSerializer {
   public:
    std::string to_json(T &) {
        std::cout << " insert serialized object here." << std::endl;
        return m_buffer.str();
    }
    template<class Arg>
    jsonSerializer &operator&(const Arg &attribute) {
        m_buffer << attribute;
        return this;
    }

   private:
    std::stringstream m_buffer;
};
}  // namespace
namespace nlohmann_test {
struct ThingToConvert {
    int a;
    float b;
};
template <class Archive>
constexpr void serialize(Archive &ar, ThingToConvert &ob, unsigned int /*version*/) {
    ar & "a:";
    ar &ob.a;
    ar & "b";
    ar &ob.b;
}
void to_json(nlohmann::json &ar, const ThingToConvert &ob) {
    ar = nlohmann::json{{"a", ob.a}, {"b", ob.b}};
}
void from_json(const nlohmann::json &ar, ThingToConvert &ob) {
    ar.at("a").get_to(ob.a);
    ar.at("b").get_to(ob.b);
}

}  // namespace nlohmann

namespace server {

/**
 * track
 * 1. types by name string
 * 2. stream connections.
 * 3. topic by name string
 * 4. n to n streams subscribed to topic.
 *      this is needed to send incoming subscriptions to
 *
 *  future work may include loop detection in the case a client is connected to
 * multiple servers, or if it becomes possible to connect a server to the same
 * router a client is also on.
 *
 *
 *  websocket server <---> each connection <---> router connection <---> local
 * subscriptions
 *
 *  or
 *
 *  websocket client <--> custom stream class <--> router
 *
 *  future work:
 *
 *  get object serialization working with nlohmann implicit conversion disabled.
 *  this makes type conversion problems easier for the compiler to catch.
 */

template <typename Key, typename Value, std::size_t Size>
struct Map {
    std::array<std::pair<Key, Value>, Size> data;
    [[nodiscard]] constexpr Value at(const Key &key) const {
        const auto itr =
            std::find_if(begin(data), end(data),
                         [&key](const auto &v) { return v.first == key; });
        if (itr != end(data)) {
            return itr->second;
        } else {
            throw std::range_error("Not Found in map");
        }
    }
};

// attempt at constexpr map
template <int mapsize>
struct MapStringInt {
    const std::array<std::pair<std::string_view, int>, mapsize> value;
    [[nodiscard]] constexpr int at(const std::string_view key) {
        constexpr auto result = std::find(value.begin(), value.end(), key);
        if constexpr (result != std::end(value)) {
            return *result;
        }
        return -1;
    }
};
/**
 * here is an example of lifting a runtime integer to a compile time integer
 * which would be helpful in the parserMessage function.
 * cf
 * https://www.reddit.com/r/cpp/comments/f8cbzs/creating_stdvariant_based_on_index_at_runtime/
 * @tparam Ts
 * @param i
 * @return
 */
template <typename... Ts>
[[nodiscard]] std::variant<Ts...> expand_type(std::size_t i) {
    assert(i < sizeof...(Ts));
    static constexpr std::variant<Ts...> table[] = {Ts{}...};
    return table[i];
}
template <typename... Ts>
[[nodiscard]] constexpr std::function<std::variant<Ts...>(std::string_view)>
expand_getter(std::size_t i) {
    // create a list of functions which parse json into a variant type.
    //  const std::function<std::variant<Ts...>(std::string_view)>> table[] =
    //  {(std::string_view msg){return
    //  nlohmann::json::parse(msg).get<Ts>();}...};
    const std::array table{std::function<std::variant<Ts...>(std::string_view)>{
        [](std::string_view msg) {
            return nlohmann::json::parse(msg).get<Ts>();
        }}...};
    static_assert(table.size() == sizeof...(Ts));
    assert(i < sizeof...(Ts));
    return table[i];
}
/**
 * NthTypeOf helper repeated here for referenece
 * template <int N, typename... Ts>
using NthTypeOf = typename std::tuple_element<N, std::tuple<Ts...>>::type;
 * @tparam VariantTypes
 * @param typeindex
 * @param message
 * @param result
 * @return
 */
template <class... VariantTypes>
void parseMessage(int typeindex, std::string_view message,
                            std::variant<VariantTypes...> &result) {
    const auto j = nlohmann::json::parse(message);
    using myVariant = std::variant<VariantTypes...>;
    static constexpr std::array<myVariant, sizeof...(VariantTypes)>
        typeArray{VariantTypes{}...};
    //  convert json to type at variant index.
    result = std::visit([j](auto v){
        myVariant v_result = j.get<decltype(v)>();
        return v_result;
    }, typeArray.at(typeindex));
}
// helper to get nth type out of
template <int N, typename... Ts>
using NthTypeOf = typename std::tuple_element<N, std::tuple<Ts...>>::type;
/** provide a helper to convert messages to a variant of messages given a topic
 * string. this requires map from topic strings to variant type index to be
 * available at compile time
 * @tparam topic_variant
 * @tparam Types
 */
template <class TopicMap, class TypeMap, typename... VariantTypes>
constexpr void staticMessageParser(
    const std::string_view topic, const std::string_view message_contents,
    const TopicMap topicMap, const TypeMap typeMap,
    std::variant<VariantTypes...> &result) {
    using namespace std::literals::string_view_literals;
    const auto index = typeMap.at(topicMap.at(topic));
    parseMessage<VariantTypes...>(index, message_contents, result);
}

// consider using recursive variadic template function to return a
// array<function<variant<>(string_view)>> of length sizeof...(Ts)
void to_json(nlohmann::json &json, const middleware::AnnounceMessage &msg) {
    json = nlohmann::json{
        {"op", msg.op()}, {"type", msg.type}, {"topic", msg.topic}};
}
void from_json(const nlohmann::json &json, middleware::AnnounceMessage &msg) {
    std::string op;
    json.at("op").get_to(op);
    if (op != msg.op()) {
        throw std::runtime_error{std::string("error: attempted converting ") +
                                 op + " to " + msg.op()};
    }
    json.at("topic").get_to(msg.topic);
    json.at("type").get_to(msg.type);
}
void to_json(nlohmann::json &json, const middleware::SubscribeMessage &msg) {
    json = nlohmann::json{
        {"op", msg.op()}, {"type", msg.type}, {"topic", msg.topic}};
}
void from_json(const nlohmann::json &json, middleware::SubscribeMessage &msg) {
    std::string op;
    json.at("op").get_to(op);
    if (op != msg.op()) {
        throw std::runtime_error(
            std::string("error while attempting to convert json message") + op +
            " to " + msg.op());
    }
    json.at("type").get_to(msg.type);
    json.at("topic").get_to(msg.topic);
}
template <typename... T>
using Message =
    std::variant<middleware::PublishMessage<T...>, middleware::SubscribeMessage,
                 middleware::AnnounceMessage>;

class PubSubRouter {
    // for each
};
/**
 * define a stream without runtime polymorphism which
 * understands pubsub messages, and connects to a local pubsub router.
 */
} /* namespace server */

using namespace std::literals::string_view_literals;

BOOST_AUTO_TEST_CASE(MapTypeLookup) {
    static constexpr std::array<std::pair<std::string_view, int>, 2>
        type_index_value{{{"number", 0}, {"floating", 1}}};
    static constexpr  server::Map<std::string_view, int, 2> typeIndex{type_index_value};
    BOOST_CHECK(typeIndex.data.size()==2u);
    // we want to parse a json string into a variant based on the attribute
    // type. the type is determined by index to variant.
    std::variant<int, float> numberResult;
    // parse a string_view into the 0th type in a variant.
    server::parseMessage(0, "1.1", numberResult);
    BOOST_TEST(numberResult.index() == 0u);
    BOOST_TEST(std::get<0>(numberResult) == 1);

    server::parseMessage(1, "1.1", numberResult);
    BOOST_TEST(numberResult.index() == 1u);
    BOOST_TEST(std::get<1>(numberResult) == (1.1f));
}
static constexpr std::array<std::pair<std::string_view, int>, 8> color_values{
    {{"black"sv, 7},
     {"blue"sv, 3},
     {"cyan"sv, 5},
     {"green"sv, 2},
     {"magenta"sv, 6},
     {"red"sv, 1},
     {"white"sv, 8},
     {"yellow"sv, 4}}};

int lookup_value(const std::string_view sv) {
    // static const auto map = std::map<std::string_view,
    // int>{color_values.begin(), color_values.end()};
    static constexpr auto map =
        server::Map<std::string_view, int, color_values.size()>{{color_values}};
    return map.at(sv);
}
BOOST_AUTO_TEST_CASE(constexprMap) {
    using namespace std::literals::string_view_literals;

    BOOST_TEST(lookup_value("red") == 1);
}
BOOST_AUTO_TEST_CASE(nlohmanImplicitCconversion2) {
    const auto initial = nlohmann_test::ThingToConvert{1, 2};
    nlohmann::json my_json;
    nlohmann_test::to_json(my_json, initial);
    using Topics = std::array<std::pair<std::string_view, std::string_view>, 3>;
    using Types = std::array<std::pair<std::string_view, int>, 3>;
    using TopicMap = server::Map<std::string_view, std::string_view, 3>;
    using TypeMap = server::Map<std::string_view, int, 3>;
    static constexpr Topics topics{{{"voltage", "float"},
                                    {"quantity", "int"},
                                    {"other_thing", "other_thing"}}};
    static constexpr Types types{
        {{"int", 0}, {"float", 1}, {"other_thing", 2}}};
    TypeMap type_map{types};
    TopicMap topic_map{topics};
    std::variant<int, float, nlohmann_test::ThingToConvert> result;
    std::cout << "attempting to parse message" <<std::endl;
    BOOST_REQUIRE_NO_THROW(server::staticMessageParser("other_thing"sv,
                                                R"({"a": 1,"b": 2.0})"sv,
                                                topic_map, type_map, result));
    BOOST_TEST(2u == result.index());
    BOOST_TEST(!result.valueless_by_exception());
    nlohmann_test::ThingToConvert t;
    BOOST_CHECK_NO_THROW(t = std::get<nlohmann_test::ThingToConvert>(result));
    BOOST_CHECK(1 == std::get<nlohmann_test::ThingToConvert>(result).a);
    BOOST_TEST(2.0 == std::get<nlohmann_test::ThingToConvert>(result).b);
    BOOST_REQUIRE_NO_THROW(server::staticMessageParser("voltage"sv, "1.0"sv, topic_map,
                                                type_map, result));
    BOOST_TEST(1u == result.index());
    BOOST_TEST(1.0 == std::get<1>(result));
}

void from_json(const nlohmann::json &document, middleware::AnnounceMessage &announce_message){
    document.at("type").get_to(announce_message.type);
    document.at("topic").get_to(announce_message.topic);
}
template<int length>
struct StaticAnnounceMessage{
    constexpr StaticAnnounceMessage():topic{}, type{}{}
    std::array<char, length> topic, type;
};

template<int length>
constexpr void from_json(const nlohmann::json &document, StaticAnnounceMessage<length> &announce_message){
    std::fill(announce_message.topic.begin(), announce_message.topic.end(), char(0));
    std::fill(announce_message.type.begin(), announce_message.type.end(), char(0));
    std::string_view temp;
    document.at("type").get_to(temp);
    std::copy(temp.begin(), temp.begin() + std::min(temp.size(), announce_message.type.size()), announce_message.type.begin());
    document.at("topic").get_to(temp);
    std::copy(temp.begin(), temp.begin() + std::min(temp.size(), announce_message.topic.size()), announce_message.topic.begin());
}
// test nlohmann constexpr speed.
// I have no idea why this even compiles since it is implemnted with nlohmann::
void modernParseAnnounce(std::string_view msg, StaticAnnounceMessage<8> &output){
    // we are kinda cheating here since we blindly assume the op type.
        using Announce = StaticAnnounceMessage<8>;
        std::variant<Announce> result;
        server::parseMessage(0, msg, result);
        output = std::get<0>(result);
}
template<int size>
constexpr StaticAnnounceMessage<size> modernParseAnnounce(std::string_view msg){
    StaticAnnounceMessage<size> result;
    modernParseAnnounce(msg, result);
    return result;
}

// slow test disabled by default [.]
BOOST_AUTO_TEST_CASE(modernMessageParsingPerformance){
    constexpr std::string_view msg =
        R"({"op":"advertise", "id":"1234567890","topic": "second star from the right and straight on until sunrise/luminosity","type":"brightness"})";
    StaticAnnounceMessage<8> am1 = modernParseAnnounce<8>(msg);
    (void) am1;
    StaticAnnounceMessage<8> announce_message;
    (void) announce_message;
#if 0
    BENCHMARK("constexpr parser"){
        modernParseAnnounce(msg, announce_message);
    };
    BENCHMARK("parse static message"){
            auto msg_parsed = nlohmann::json::parse(msg);
            (void) msg_parsed;
    };
#endif
}
