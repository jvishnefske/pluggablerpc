macro(set_base_compile_option)
    option(BUILD_SHARED_LIBS "shared middleware libraries" OFF)
    if(MSVC)
        foreach (flag_var
                CMAKE_C_FLAGS CMAKE_C_FLAGS_DEBUG CMAKE_C_FLAGS_RELEASE
                CMAKE_C_FLAGS_MINSIZEREL CMAKE_C_FLAGS_RELWITHDEBINFO
                CMAKE_CXX_FLAGS CMAKE_CXX_FLAGS_DEBUG CMAKE_CXX_FLAGS_RELEASE
                CMAKE_CXX_FLAGS_MINSIZEREL CMAKE_CXX_FLAGS_RELWITHDEBINFO)
            string(REPLACE "/W3" "/W4" ${flag_var} "${${flag_var}}")
            string(REPLACE "/W1" "/W4" ${flag_var} "${${flag_var}}")
        endforeach()
#        set(cxx_base_flags "-GS -W4 -WX -wd4251 -wd4275 -nologo -J")
#        set(cxx_base_flags "${cxx_base_flags} -D_UNICODE -DUNICODE -DWIN32 -D_WIN32")
#        set(cxx_base_flags "${cxx_base_flags} -DSTRICT -DWIN32_LEAN_AND_MEAN")
    elseif(CMAKE_CXX_COMPILER_ID MATCHES "Clang*")
        set(cxx_base_flags "-WError=all -Wshadow -Wconversion")
    elseif (CMAKE_COMPILER_IS_GNUCXX)
        set(cxx_base_flags "-Wall -Wshadow")
    endif()
endmacro()

macro(warning_flags target)
    # visual studio
    if( "${CMAKE_CXX_COMPILER_ID}" STREQUAL "MSVC")
        message(STATUS "detected visual studio")
        set(WARNING_FLAGS /W4)
        # remove /W3 from CMAKE_CXX_FLAGS
        #        target_compile_features(${target} PRIVATE )
        target_compile_options(${target} PRIVATE /W4)
        #        add_compile_options(${WARNING_FLAGS})
    elseif("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU" OR "${CMAKE_CXX_COMPILER_ID}" MATCHES "Clang.*" )
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Werror=all -Werror=extra")
    else()
        message(WARNING "unknown compiler #${CMAKE_CXX_COMPILER_ID}#")
    endif()
endmacro()

macro(_sanitize target)
    # msvc
    if( "${CMAKE_CXX_COMPILER_ID}" STREQUAL "MSVC")
        # This is an issue where the msvc linker hangs forever on test.exe.
        message(STATUS "ubsan on windows seems broken at the moment")
#        target_compile_options(${target} PRIVATE  /fsanitize=address )
        # this is avaliable on very new MSVC compilers.
        # target_compile_options(${target} PUBLIC /fsanitize=fuzzer)
        # turn on thread warnings in msvc
        # target_compile_options(${target} PUBLIC /w14640 /GS /EHsc /Zi /fsanitize-address-use-after-return /fno-sanitize-address-vcasan-lib)
    else()
        # consider substituting address san with thread sanitizer as needed for develpment.
        if(${CMAKE_SYSTEM_PROCESSOR} STREQUAL "x86")
            target_compile_options(${target} PRIVATE -fsanitize=undefined -fsanitize=address)
        else()
            message(WARNING "ubsan may not be supported on ${CMAKE_SYSTEM_PROCESSOR}")
        endif()
#        target_compile_options(${target} PRIVATE -fsanitize=address -fsanitize=undefined -fno-omit-frame-pointer)
#        target_link_options(${target} PRIVATE -fsanitize=address -fsanitize=undefined -fno-omit-frame-pointer)
    endif()
endmacro()
macro(sanitize_only_if_debug target)
    if(MIDDLEWARE_ENABLE_SANITIZE)
    if(${CMAKE_BUILD_TYPE} MATCHES ".*Debug.*")
        message(STATUS "Enabling sanitization since debug is enabled.")
        _sanitize(${target})
    else()
        # consider adding -pie, and -fstack-protector-all, and on msvc
        if(MSVC)
            message(STATUS "Enabling aslr, and stack protection")
            target_compile_options(${target} PRIVATE -WI,nxcompat)
        else()
            message(STATUS "Enabling data execution protector")
            target_compile_options(${target} PRIVATE -Fpie -fstack-protector-all)
        endif()
        message(STATUS "skipping sanitization since debug is not detected: ${CMAKE_BUILD_TYPE}.")
    endif()
    endif()
endmacro()
macro(addInstall TARGETS)
    message(STATUS "adding install export for ${TARGETS}")
    install(TARGETS ${TARGETS}
            EXPORT ${TARGETS}
            RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
            LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
            PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_INCLUDEDIR})
    install(EXPORT ${TARGETS} DESTINATION TargetInfo)
endmacro()
macro(dump_extra_info)
    message(STATUS "env flags ${CXXFLAGS}")
    message(STATUS "FLAGS: ${CMAKE_CXX_FLAGS}")
    message(STATUS "available features: ${CMAKE_CXX_COMPILE_FEATURES}")
    message(STATUS "available c features: ${CMAKE_C_COMPILE_FEATURES}")
endmacro()