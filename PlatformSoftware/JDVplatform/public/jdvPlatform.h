//
// Created by John Vishnefske on 4/2/21.
//

#ifndef FAULT_TOLERANT_JDVPLATFORM_H
#define FAULT_TOLERANT_JDVPLATFORM_H
#include <string>

namespace jdv{
    typedef std::string Filename;
    typedef uint64_t U64;
    typedef uint32_t U32;
    typedef int64_t I64;
    typedef int32_t I32;
}
#endif //FAULT_TOLERANT_JDVPLATFORM_H
