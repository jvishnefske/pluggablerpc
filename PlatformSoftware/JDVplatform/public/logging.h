//
// Created by John Vishnefske on 4/2/21.
//

#ifndef FAULT_TOLERANT_LOGGING_H
#define FAULT_TOLERANT_LOGGING_H

namespace jdv {
    enum loglevel {
        kTRACE,
        kDEBUG,
        kINFO,
        kWARNING,
    };
    constexpr std::iostream log(const enum loglevel=kINFO, std::string message="") {
        std::cout << loglevel <<  ": " << std::endl;
        return std::cout;
    };
} //end namespace
#endif //FAULT_TOLERANT_LOGGING_H
