//
// Created by John Vishnefske on 4/2/21.
//

#ifndef FAULT_TOLERANT_CONFIGFILE_H
#define FAULT_TOLERANT_CONFIGFILE_H
#include <string>

namespace jdv {
    struct ConfigItem{
        std::string name;
        std::string description;
        bool hasValue;
    };
/**
 * this class allows reading configuration from a file, environment variables,
 * or command line arguments
 */
    class ConfigReader {
    public:
        ConfigReader(ConfigItem[]);

        operator[](std::string attrib) {

        }

    private:
    };

} // end namespace


#endif //FAULT_TOLERANT_CONFIGFILE_H
