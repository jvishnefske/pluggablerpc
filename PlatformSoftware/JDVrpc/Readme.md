# rpc interface
allows both dynamic, and staticly geneerated rpc servers

# future considered features:
- define data schemas at runtime in text.
- discover new schemas over the network
- pub sub values within c++ classes.
- discover remote services.
- auto failover, and reconnect after network disruption
- pluggable backends (ie websocket/zeromq)
